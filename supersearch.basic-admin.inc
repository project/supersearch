<?php

/**
 * @file
 * Admin page callbacks for the supersearch module.
 */

/**
 * Menu callback: confirm wiping of the index.
 */
function supersearch_wipe_confirm() {
  return confirm_form(array(), t('Are you sure you want to re-index the site?'),
                  'admin/settings/supersearch', t(' The search index is not cleared but systematically updated to reflect the new settings. Searching will continue to work but new content will not be indexed until all existing content has been re-indexed. This action cannot be undone.'), t('Re-index site'), t('Cancel'));
}

/**
 * Handler for wipe confirmation
 */
function supersearch_wipe_confirm_submit(&$form, &$form_state) {
  if ($form['confirm']) {
    supersearch_wipe();
    drupal_set_message(t('The index will be rebuilt.'));
    $form_state['redirect'] = 'admin/settings/supersearch';
    return;
  }
}

function supersearch_get_indexing_status_description()
{
 $remaining = 0;
  $total = 0;
  foreach (module_list() as $module) {
    if (module_hook($module, 'search')) {
      $status = module_invoke($module, 'search', 'status');
      $remaining += $status['remaining'];
      $total += $status['total'];
    }
  }
  //$remaining = rand(9,$total);
  $count = format_plural($remaining, 'There is 1 item left to index out of %total.', 'There are @count items left to index out of %total.');
  $percentage = ((int)min(100, 100 * ($total - $remaining) / max(1, $total))) .'%';
  return t('%percentage of the site has been indexed.', array('%percentage' => $percentage)) .' '. t($count, array('%total' =>  $total)) ;
}

function supersearch_get_indexing_stats()
{
  drupal_json(array('status'=>supersearch_get_indexing_status_description()));
}


/**
 * Menu callback; displays the search module settings page.
 *
 * @ingroup forms
 * @see system_settings_form()
 * @see search_admin_settings_validate()
 */
function supersearch_admin_settings() {
  drupal_add_js(drupal_get_path('module', 'supersearch') . '/admin-indexing-counter.js');
  drupal_add_js("var path_to_indexer_status_callback = '" .  url('admin/settings/supersearch/indexing-stats') . "';", 'inline');
  $status = '<div id="indexing-stats" style="margin-top: 8px; margin-bottom: 8px;">'. supersearch_get_indexing_status_description().'</div>';
  $form['status'] = array('#type' => 'fieldset', '#title' => t('Indexing status'));
  $form['status']['status'] = array('#value' => $status);
  $form['status']['wipe'] = array('#type' => 'submit', '#value' => t('Re-index site'), '#submit' => array('_ss_reindex'));

  $min_items = drupal_map_assoc(range(0, 20));
  $sec_items = drupal_map_assoc(range(0, 60));

  // Indexing throttle:
  $form['indexing_throttle'] = array('#type' => 'fieldset', '#title' => t('Batch indexing throttle'));
  //$form['indexing_throttle']['search_cron_time_limit'] = array('#type' => 'select', '#title' => t('Number of minutes to let the indexer run between cron calls'), '#default_value' => variable_get('search_cron_time_limit', 5), '#options' => $items, '#description' => t('To avoid indexing runs overlapping each other and taking up system resources please set this number below the interval that you have set between cron calls.'));

  $search_cron_time_limit = variable_get('search_cron_time_limit', 50);
  $search_cron_time_limit_mins = (integer)($search_cron_time_limit/60);
  $search_cron_time_limit_secs = $search_cron_time_limit - $search_cron_time_limit_mins * 60;

/*
  $form['indexing_throttle']['search_cron_time_limit']['search_cron_time_limit'] = array('#type' => 'markup', '#value' => '<div style="font-weight: bold">' . t('Time to let the indexer run between cron calls:') . '</div>');
  $form['indexing_throttle']['search_cron_time_limit']['search_cron_time_limit_mins'] = array('#type' => 'textfield', '#title' => t('Minutes'), '#default_value' => $search_cron_time_limit_mins, '#size' => 2, '#maxlength' => 2, '#prefix'=> '<table style="width: 200px; border: none;"><tr><td>', '#suffix' => '</td>', '#element_validate' => array('_ss_time_element_validate'));
  $form['indexing_throttle']['search_cron_time_limit']['search_cron_time_limit_secs'] = array('#type' => 'textfield', '#title' => t('Seconds'), '#default_value' => $search_cron_time_limit_secs, '#size' => 2, '#maxlength' => 2, '#prefix'=> '<td>', '#suffix' => '</td></tr></table>', '#element_validate' => array('_ss_time_element_validate', '_ss_totalruntime_validate'));
  $form['indexing_throttle']['search_cron_time_limit']['search_cron_time_limit_legend'] = array('#type' => 'markup', '#value' => '<div style="font-style: italic">' . t('To avoid indexing runs overlapping each other and taking up system resources please set this number below the interval that you have set between cron calls.') . '</div>');
*/

/*
  $form['indexing_throttle']['search_cron_time_limit'] = array(
	'#type' => 'fieldset',
	'#title' => t('Time to let the indexer run between cron calls:'),
	'#description' => t('To avoid indexing runs overlapping each other and taking up system resources please set this number below the interval that you have set between cron calls.'),
  );
*/
  $form['indexing_throttle']['#description'] = t('<strong>Time to let the indexer run between cron calls.</strong> To avoid indexing runs overlapping each other and taking up system resources please set this number below the interval that you have set between cron calls.');
  $form['indexing_throttle']['search_cron_time_limit']['search_cron_time_limit_mins'] = array(
	'#type' => 'textfield',
	'#title' => t('Minutes'),
	'#default_value' => $search_cron_time_limit_mins,
	'#size' => 2,
	'#maxlength' => 2,
	'#prefix'=> '<div style="float:left;margin:0 0.5em;">',
	'#suffix' => '</div>',
	'#element_validate' => array('_ss_time_element_validate'),
  );
  $form['indexing_throttle']['search_cron_time_limit']['search_cron_time_limit_secs'] = array(
	'#type' => 'textfield',
	'#title' => t('Seconds'),
	'#default_value' => $search_cron_time_limit_secs,
	'#size' => 2,
	'#maxlength' => 2,
	'#prefix'=> '<div style="float:left;margin:0 0.5em;">',
	'#suffix' => '</div>',
	'#element_validate' => array('_ss_time_element_validate', '_ss_totalruntime_validate'),
  );

  $form['#submit'][] = 'supersearch_admin_settings_submit';
 // Per module settings
  $form = array_merge($form, module_invoke_all('search', 'admin'));      

  return system_settings_form($form);
}


function _ss_time_element_validate($element, &$form_state)
{
  $value = (int)$element['#value'];
  if (($value < 0) OR ($value >= 60)) form_error($element, t('This number needs to be between 0 and 59'));
}

function _ss_get_maximum_execution_time()
{
  $value = (int)ini_get('max_execution_time');
  if ($value == 0) return 3600; else return min($value, 3600);
}

function _ss_totalruntime_validate($element, &$form_state)
{
//cleanDump($form_state);
 $total_run_time = (int)$form_state['values']['search_cron_time_limit_mins'] * 60 + (int)$form_state['values']['search_cron_time_limit_secs']; 
 if ($total_run_time <= 0) form_error($element, t('The total run time cannot be zero; we suggest a minimum run time of 50 seconds.'));
 $max_execution_time = _ss_get_maximum_execution_time();
 if ($total_run_time > $max_execution_time) form_error($element, t('The total run time cannot exceed the maximum execution time of @count seconds defined in your php.ini file.', array('@count' => $max_execution_time)));
 else
 {
   $safety_gap = abs($max_execution_time - $total_run_time);
   echo ($safety_gap < 30);
   if ($safety_gap < 30) form_error($element, t('The total run time is too close to the maximum execution time of @count seconds defined in your php.ini file; you should set the total node indexing time to be at least 30 seconds less than the maximum run time so that the clean-up functions and other search indexing operations be given time to complete.', array('@count' => $max_execution_time)));
 }
}

function _ss_reindex($form, &$form_state)
{
  drupal_goto('admin/settings/supersearch/wipe');
}

/**
 * Validate callback.
 */
function supersearch_admin_settings_validate($form, &$form_state) {
  //if ($form_state['values']['op'] == t('Re-index site')) {
    //drupal_goto('admin/settings/supersearch/wipe');
  //}  
}

function supersearch_admin_settings_submit($form, &$form_state) {
  variable_set('search_cron_time_limit', $form_state['values']['search_cron_time_limit_mins']*60 + $form_state['values']['search_cron_time_limit_secs']);
}