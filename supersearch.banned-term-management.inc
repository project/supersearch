<?php

function supersearch_banned_term_management_settings() {
  $form['banned_term_basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Banned Term Management Basic Settings'),
  );

  $form['banned_term_basic']['ban_term_not_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Banned terms cannot be searched for (they should return an empty result set).'),
    '#default_value' => variable_get('ban_term_not_search', TRUE),
  );

  $form['banned_term_basic']['ban_term_not_visible_TopX'] = array(
    '#type' => 'checkbox',
    '#title' => t('Banned terms are never visible in the TopX listing.'),
    '#default_value' => variable_get('ban_term_not_visible_TopX', TRUE),
  );

  $form['banned_term'] = array(
    '#type' => 'fieldset',
    '#title' => t('Banned Terms'),
  );

  $form['banned_term']['table'] = array(
    '#type' =>'markup',
    '#value' => _supersearch_return_banned_terms_table(),
  );

  $form['banned_term']['banned_term_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Banned Term'),
    '#size' => 20,
    '#maxlength' => 64,
    '#description' => t(''),
    '#attributes' => array('style' => 'width:150px'),
  );

  $form['banned_term']['banned_term_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Banned Term'),
    '#ahah' => array(
       'path' => 'admin/settings/supersearch/banned-terms/get-table',
       'event' => 'click',
       'wrapper' => 'banned-terms-table',
       'progress' => array('type' => 'bar', 'message' => t('Please wait...')),
     ),
  );

  return system_settings_form($form);
}

function _supersearch_return_banned_terms_table()
{
    $items = _supersearch_return_banned_terms();

    $header = array(t('Term'), t('Operations'));

    $rows = array();
    foreach ($items as $key=>$item) {
        $row = array();
        $row[0] = check_plain($item['terms']);
        //$row[1] = check_plain($item['nid']);
        $row[1] = l(t('Remove Association'), 'admin/settings/supersearch/banned-terms/remove-association/' . $item['id']);
        //$row[2] = l(t('Remove Association'), 'admin/settings/supersearch/enriched-terms/remove-association/' . $item['id']) . ' | ' . l(t('View Node'), 'node/' . $item['nid'], array('attributes' => array('target' => '_blank')));

        $rows[] = $row;
        unset($row);
    }

  return '<div id="banned-terms-table">' .theme('table', $header, $rows) . '</div>';
}

function _supersearch_return_banned_terms() {
    $result = db_query("SELECT * FROM {supersearch_terms_options} WHERE options = %d ORDER BY terms ASC", SUPERSEARCH_BANNED_TERMS);
    $terms = array();
    while ($row = db_fetch_object($result))
        $terms[$row->terms] = array("id" => $row->id, "terms" => $row->terms, "type" => "banned");
    return $terms;
}

/*
function _supersearch_return_banned_terms() {
    $result = db_query("SELECT * FROM {supersearch_banned_terms} ORDER BY terms ASC");
    $terms = array();
    while ($row = db_fetch_object($result))
        $terms[$row->terms] = array("terms" => $row->terms, "id" => $row->id);
    return $terms;
}
*/

function supersearch_get_banned_term_table_ajax()
{
    $terms = $_POST['banned_term_name'];
    $message = supersearch_add_banned_term($terms);
    drupal_json(array('status' => TRUE, 'data' => $message . _supersearch_return_banned_terms_table()));
  exit;
}

function supersearch_add_banned_term($terms)
{
   $terms = trim($terms);
   if (strlen($terms) == 0) return '<br><div style="color: red">'. t('Banned Term field is empty') . '</div>';;
   db_query("DELETE FROM {supersearch_terms_options} WHERE terms='%s' AND options=%d", $terms, SUPERSEARCH_SEO_TERMS);

   $result = db_query_range("SELECT * FROM {supersearch_terms_options} WHERE terms='%s' AND options = %d", $terms, SUPERSEARCH_BANNED_TERMS, 0, 1);
   if(!db_affected_rows($result)){
     $message = '<br><div style="color: red">'. t('Term "@term" is added', array('@term'=>$terms)) . '</div>';
     $row = new stdClass();
     $row->terms = $terms;
     drupal_write_record('supersearch_terms_options', $row);
   }
   else{
     $message = '<br><div style="color: red">'. t('Term "@term" is dublicate', array('@term'=>$terms)) . '</div>';
   }
   return $message;
}

function supersearch_banned_term_table_delete_item($id)
{
 db_query("DELETE FROM {supersearch_terms_options} WHERE id=%d", $id);
 drupal_goto('admin/settings/supersearch/banned-term-management');
}