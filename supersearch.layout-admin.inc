<?php



function _supersearch_return_enriched_terms_table()
{
  $items = _supersearch_get_enriched_terms();

    $header = array(t('Term'),t('Node ID'),t('Operations'));

    $rows = array();
    foreach ($items as $key=>$item) {
        $row = array();
        $row[0] = check_plain($item['terms']);
        $row[1] = check_plain($item['nid']);
        $row[2] = l(t('Remove Association'), 'admin/settings/supersearch/enriched-terms/remove-association/' . $item['id']) . ' | ' . l(t('View Node'), 'node/' . $item['nid'], array('attributes' => array('target' => '_blank')));

        $rows[] = $row;
        unset($row);
    }

  return '<div id="enriched-terms-table">' .theme('table', $header, $rows) . '</div>';

}

function _supersearch_return_templated_terms_table()
{
  $result = db_query("SELECT * FROM {search_term_template_assoc}");

  $header = array(t('Term'),t('Template Name'),t('Operations'));
   $rows = array();
    
    while ($item = db_fetch_object($result)) {
        $row = array();
        $row[0] = check_plain($item->keyword);
        $row[1] = check_plain($item->template_name);
        $row[2] = l(t('Remove Association'), 'admin/settings/supersearch/templated-terms/remove-association/' . $item->id);

        $rows[] = $row;
        unset($row);
    }
     return '<div id="templated-terms-table">' .theme('table', $header, $rows) . '</div>';

}


function supersearch_enriched_term_table_delete_item($id)
{
 db_query("DELETE FROM {supersearch_help_nodes} WHERE id=%d", $id);
 drupal_goto('admin/settings/supersearch/layout-and-presentation');
}

function supersearch_templated_term_table_delete_item($id)
{
 db_query("DELETE FROM {search_term_template_assoc} WHERE id=%d", $id);
 drupal_goto('admin/settings/supersearch/layout-and-presentation');
}


function supersearch_layout_settings() {

  $form['top_terms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic terms presentation settings'),
  );

  $form['top_terms']['top_terms_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Adv. Search panel terms count (without added by admin)'),
    '#default_value' => variable_get('top_terms_count', 3),
    '#size' => 5,
    '#maxlength' => 3,
    '#description' => t('Number of Top terms links on Advanced Search panel added by statistics')
  );

  $form['top_terms']['top_terms_count_topX'] = array(
    '#type' => 'textfield',
    '#title' => t('TopX panel terms count (without added by admin)'),
    '#default_value' => variable_get('top_terms_count_topX', 10),
    '#size' => 5,
    '#maxlength' => 3,
    '#description' => t('Number of Top terms links on TopX panel')
  );

  $form['top_terms']['top_terms_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Top terms title'),
    '#default_value' => variable_get('top_terms_title', t('Top searches')),
    '#size' => 30,
    '#attributes' => array('style' => 'width:135px'),
  );

  $options = array(TOP_RESULTS_SORT_ASC_RANK => "Ascending rank mode", TOP_RESULTS_SORT_DESC_RANK => "Descending rank mode", TOP_RESULTS_SORT_ALPHA => "Alphabetical sort mode");
  $form['top_terms']['top_terms_sort_type'] = array(
    '#title' => t('Terms view'),
    '#type' => 'select',
    '#options' => $options,
    //'#attributes' => array('onchange' => $script),
    '#default_value' => variable_get('top_terms_sort_type', TOP_RESULTS_SORT_DESC_RANK),
  );

  if(module_exists('supersearch_seo')) {
    $script = "var num = $('#edit-top-terms-sort-type').val(); $('#edit-search-terms-list-region-wrapper').css('display', num != " . TOP_RESULTS_SORT_ALPHA . " ? 'block' : 'none')";
    $form['top_terms']['top_terms_sort_type']['#attributes']['onchange'] = $script;
    $options = array(TOP_RESULTS_SEO_REGION_BEGIN => "At the begin of list", TOP_RESULTS_SEO_REGION_END => "At the end of list");
    $form['top_terms']['search_terms_list_region'] = array(
      '#type' => 'select',
      '#title' => t('Region of SEO terms'),
      '#options' => $options,
      '#default_value' => variable_get('search_terms_list_region', TOP_RESULTS_SEO_REGION_END),
      '#attributes' => array('style' => 'width:140px'),
      '#description' => t('This feature work if Terms view isn\'t Alphabetical sort mode'),
    );
    drupal_add_js('$(document).ready(function() { ' . $script . ' });', 'inline');
  }

    $form['search_history'] = array('#type' => 'fieldset', '#title' => t('Search History'));
    $form['search_history']['search_history_enabled'] = array('#type' => 'checkbox', '#title' => t('Enable the display of search history'), '#default_value' => variable_get('search_history_enabled', TRUE), '#description' => t("Users with the proper permissions will see a list of their most recent search terms"));
    $form['search_history']['search_history_count'] = array('#type' => 'textfield', '#title' => t('Maximum number of search queries to show'), '#default_value' => variable_get('search_history_count', 5), '#size' => 5, '#maxlength' => 3, );
    $form['search_history']['search_history_asc'] = array('#type' => 'select', '#title' => t('Search history view'), '#default_value' => variable_get('search_history_asc', 1), '#options' => array(0 => t("Most recent query last"), 1 => t("Most recent query first")), );


    $form['results_view_weigths'] = array('#type' => 'fieldset', '#title' => t('Search Results Page Order Settings'), '#theme' => 'results_view_weigths');

    $search_types = array();
    $weigths = get_search_results_elem_weigths();

    foreach (module_implements('search') as $name) {
        $search_type = module_invoke($name, 'search', 'name', TRUE);
        if (empty($search_type))
            continue;

        $search_types[$name] = $search_type;

        if (!isset($weigths[$name]))
            $weigths[$name] = $weigths['default'];
        $weigth=0;
        foreach ($weigths[$name] as $item) {
            $form['results_view_weigths'][$name]['values'][$item]['name'] = array('#value' => $item);
            $form['results_view_weigths'][$name]['values'][$name . '-weigth-' . $item] = array('#type' => 'textfield', '#size' => 5, '#default_value' => $weigth, '#attributes' => array('class' => 'weight'));
            $weigth++;
        }
    }

    $script = "var num = $('#edit-type option:selected').val(); $('.supersearch-search-table').css('display', 'none'); $('#supersearch-search-table-'+num).css('display', 'block');";

    $form['results_view_weigths']['type'] = array(
        '#type' => 'select',
        '#title' => t('Select search type'),
        '#options' => $search_types,
        '#attributes' => array('onchange' => $script),
    );

/*
  $form['results_view_weigths']['search_search_type_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('search_search_type_submit'),
  );
*/

    drupal_add_js('$(document).ready(function() { ' . $script . ' });', 'inline');



    $form['search_enrichment'] = array(
        '#type' => 'fieldset',
        '#title' => t('Search Enrichment'),
        '#collapsible' => TRUE,
    );

    $form['search_enrichment']['description'] = array(
        '#type' => 'markup',
        '#value' => '<div>' . t('This feature lets you preface certain searches with custom information; it could be the bio of a person searched for, a list of links related to the topic, a definition, anything. It works by associating node IDs with terms. These nodes need not be publicly-accessible to be published here.') . '</div>',
    );

   
  $form['search_enrichment']['table'] = array('#type' =>'markup', '#value' => _supersearch_return_enriched_terms_table());



    $form['search_enrichment']['search_term_enriched'] = array(
        '#type' => 'textfield',
        '#title' => t('Search Term'),
        '#size' => 20,
        '#maxlength' => 64,
        '#description' => t(''),
        '#attributes' => array('style' => 'width:150px'),
    );

    $form['search_enrichment']['help_node_nid'] = array(
        '#type' => 'textfield',
        '#title' => t('Help Node Id'),
        '#size' => 10,
        '#description' => t(''),
        '#attributes' => array('style' => 'width:150px'),
    );


    $form['search_enrichment']['help_node_save'] = array(
        '#type' => 'submit',
        '#value' => t('Save Node Association'),
        '#submit' => array('add_new_help_node_submit'),
        '#validate' => array('add_new_help_node_validate'),
        '#ahah' => array(
          'path' => 'admin/settings/supersearch/enriched-terms/get-table',
          'event' => 'click',
          'wrapper' => 'enriched-terms-table',
          'progress' => array('type' => 'bar', 'message' => t('Please wait...')),
        ),

    );




    $items = array();
    $result = db_query("SELECT keyword FROM {search_term_template_assoc} ORDER BY keyword ASC");
    while ($row = db_fetch_object($result)) {
        $items[$row->keyword] = $row->keyword;
    }

    $form['results_keys_templates'] = array('#type' => 'fieldset', '#title' => t('Assign templates to terms'), '#collapsible' => TRUE, );

    $form['results_keys_templates']['table'] = array('#type' =>'markup', '#value' => _supersearch_return_templated_terms_table());


    $form['results_keys_templates']['search_term_templated'] = array(
        '#type' => 'textfield',
        '#title' => t('Term name'),
        '#size' => 10,
        '#description' => t("To assign more then one term, please separate them with a comma."),
        '#attributes' => array('style' => 'width:135px'),
    );

    $form['results_keys_templates']['search_template'] = array(
        '#type' => 'textfield',
        '#title' => t('Template name'),
        '#size' => 10,
        '#description' => t(''),
        '#attributes' => array('style' => 'width:135px'),
    );

    $form['results_keys_templates']['search_term_assoc_template_save'] = array(
        '#type' => 'submit',
        '#value' => t('Save Template Association'),
        '#validate' => array('add_new_term_validate'),
        '#submit' => array('add_new_term_submit'),
        //new
        '#ahah' => array(
        'path' => 'admin/settings/supersearch/templated-terms/get-table',
        'event' => 'click',
        'wrapper' => 'templated-terms-table',
        'progress' => array('type' => 'bar', 'message' => t('Please wait...')),
        ),
    );

    
    $form['additional_options'] = array('#type' => 'fieldset', '#title' => t('Additional Options'));
    $form['additional_options']['sponsor_links_disabled'] = array('#type' => 'checkbox', '#title' => t('Disable Sponsor Link'), '#default_value' => variable_get('sponsor_links_disabled', FALSE), '#description' => t('Search results pages will display, at the bottom, a link to our company as a courtesy for investing hundreds of man-hours in this module. This will have little or no effect on your ranking, but will make our company easier to find.'));

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#submit' => array('search_search_type_submit'),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save changes'),
        '#submit' => array('supersearch_layout_settings_submit'),
    );

    return $form;
}

function supersearch_add_enriched_term($nid, $terms)
{
   $terms = trim($terms);
   if (strlen($terms) == 0) return;
   db_query("DELETE FROM {supersearch_help_nodes} WHERE terms='%s'", $terms);
        $row = new stdClass();
        $row->nid = $nid;
        $row->terms = $terms;
        drupal_write_record('supersearch_help_nodes', $row);
}

function supersearch_add_templated_term($terms, $template)
{
   $terms = trim($terms);
   if (strlen($terms) == 0) return;
   if (strlen($template) == 0) return;
   db_query("DELETE FROM {search_term_template_assoc} WHERE keyword='%s'", $terms);
        $row = new stdClass();
        $row->keyword = $terms;
        $row->template_name = $template;
        drupal_write_record('search_term_template_assoc', $row);
}

function supersearch_get_templated_term_table_ajax()
{
    $terms = $_POST['search_term_templated'];
    $template = $_POST['search_template'];
        $message = '';
        supersearch_add_templated_term($terms, $template);

    drupal_json(array('status' => TRUE, 'data' => $message . _supersearch_return_templated_terms_table()));
  exit;
}

function supersearch_get_enriched_term_table_ajax()
{
    $terms = $_POST['search_term_enriched'];
    $nid = $_POST['help_node_nid'];
    if (!node_load($nid, NULL, TRUE)) $message = '<br><div style="color: red">'. t('Node @nid does not exist', array('@nid'=>$nid)) . '</div>'; else
    {
        $message = '';
        supersearch_add_enriched_term($nid, $terms);

    }

    drupal_json(array('status' => TRUE, 'data' => $message . _supersearch_return_enriched_terms_table()));
  exit;
}


function _supersearch_get_enriched_terms() {
    $result = db_query("SELECT * FROM {supersearch_help_nodes} ORDER BY terms ASC");
    $terms = array();
    while ($row = db_fetch_object($result))
        $terms[$row->terms] = array("nid" => $row->nid, "terms" => $row->terms, "id" => $row->id);
    return $terms;
}

function add_new_help_node_validate($form, &$form_state) {
    $nid = $form_state['values']['help_node_nid'];
    if (!node_load($nid, NULL, TRUE)) form_set_error($form['search_enrichment']['help_node_nid'], t('Node @nid does not exist', array('@nid'=>$nid)));

}

function add_new_help_node_submit($form, &$form_state) {

    $nid = $form_state['values']['help_node_nid'];
    $terms = $form_state['values']['search_term_enriched'];
    if (!node_load($nid, NULL, TRUE)) drupal_set_message('Node does not exist', 'error');
    else supersearch_add_enriched_term($nid, $terms);
        
    

}


function supersearch_layout_settings_submit($form, &$form_state) {
    variable_set('top_terms_count', $form_state['values']['top_terms_count']);
    if(variable_get('top_terms_count_topX', 10) != $form_state['values']['top_terms_count_topX']) {
      variable_set('top_terms_count_topX', $form_state['values']['top_terms_count_topX']);
      menu_rebuild();
      if (module_exists('xmlsitemap')) {
        xmlsitemap_delete_cache_files();
      }
    }
    //variable_set('top_terms_count_topX', $form_state['values']['top_terms_count_topX']);
    variable_set('top_terms_title', $form_state['values']['top_terms_title']);
    variable_set('top_terms_sort_type', $form_state['values']['top_terms_sort_type']);
    variable_set('search_terms_list_region', $form_state['values']['search_terms_list_region']);

    $change_to_layout = ((variable_get('search_history_enabled', TRUE) != $form_state['values']['search_history_enabled']) || (variable_get('sponsor_links_disabled', FALSE) != $form_state['values']['sponsor_links_disabled']) );
    variable_set('search_history_enabled', $form_state['values']['search_history_enabled']);
    variable_set('search_history_count', $form_state['values']['search_history_count']);
    variable_set('search_history_asc', $form_state['values']['search_history_asc']);
    variable_set('sponsor_links_disabled', $form_state['values']['sponsor_links_disabled']);

    if ($change_to_layout) supersearch_results_cache_clear();

    $weigths = array();
    foreach ($form['results_view_weigths']['type']['#options'] as $name => $value) {
        $index = $name . "-weigth-";
        $weigths[$name] = array();
        foreach ($form_state['values'] as $key => $data) {
            if (strpos($key, $index) === 0) {
                $id = drupal_substr($key, drupal_strlen($index));
                $weigths[$name][$data] = $id;
            }
        }
        ksort($weigths[$name]);
    }

    $weigths['default'] = get_results_page_elems_default_weigths();
    variable_set('results_elems_weigths', $weigths);

    $weigths = array();
    foreach ($form['results_view_weigths']['type']['#options'] as $name => $value) {
        $index = $name . "-weigth-";
        $weigths[$name] = array();
        foreach ($form_state['values'] as $key => $data) {
            if (strpos($key, $index) === 0) {
                $id = drupal_substr($key, drupal_strlen($index));
                $weigths[$name][$data] = $id;
            }
        }
        ksort($weigths[$name]);
    }

    $weigths['default'] = get_results_page_elems_default_weigths();
    variable_set('results_elems_weigths', $weigths);
    return;
}





function supersearch_term_template_add_assoc($unset = FALSE) {
    $assoc_terms = explode(",", $_POST["search_term"]);
    $assoc_template = $_POST["search_template"];
    $return = '';
    foreach ($assoc_terms as $assoc_term) {
        $assoc_term = trim($assoc_term);
        $old_row = db_fetch_object(db_query_range("SELECT * FROM {search_term_template_assoc} WHERE keyword='%s'", $assoc_term, 0, 1));

        if ($unset || !trim($assoc_template)) {
            db_query("DELETE FROM {search_term_template_assoc} WHERE id=%d", $old_row->id);
            $return .= "<script>$('#edit-search-term-assoc-template-list option[@value=$assoc_term]').remove();$('#edit-search-term').attr('value','');$('#edit-search-template').attr('value','')</script>";
            $act = -1;
        }
        else {
            $row = new stdClass();
            $row->keyword = $assoc_term;
            $row->template_name = $assoc_template;
            if ($old_row) {
                $row->id = $old_row->id;
                drupal_write_record('search_term_template_assoc', $row, 'id');
            }
            else {
                drupal_write_record('search_term_template_assoc', $row);
                $return1 = "<option value={$assoc_term}>{$assoc_term}</option>";
                $return .= "<script>$('#edit-search-term-assoc-template-list').append('{$return1}');</script>";
            }
            $act = 1;
        }
    }

    if ($act == 1) {
        $return .= "<script>$('#edit-search-term-assoc-template-list option:last').attr('selected', 'selected');$('#edit-search-term-assoc-template-list').change()</script>";
        $return .= "Value Saved";
    }
    else
        $return .= "Value Deleted";

    print "{ \"status\": true, \"data\": \"" . $return . "\" }";
    exit;
}

function theme_results_view_weigths($form) {
    $output = drupal_render($form['type']);
    unset($form['type']);

    foreach ($form as $search_name => $values) {
        if (is_array($values)&&isset($values['values'])) {
            $header = array("Order on the page", "Weigth");
            $table_rows = array();
            foreach ($values['values'] as $name => $value) {
                if (isset($value['name']['#value'])) {
                    $table_rows[] = array('data' => array(drupal_render($value['name']), drupal_render($values['values'][$search_name . '-weigth-' . $value['name']['#value']])), 'class' => 'draggable');
                }
            }
            unset($form[$search_name]);
            $output .= "<div id = 'supersearch-search-table-" . $search_name . "' class = 'supersearch-search-table'>";
            drupal_add_tabledrag('supersearch-search-type-' . $search_name, 'order', 'sibling', 'weight');
            $output .= theme('table', $header, $table_rows, array('id' => 'supersearch-search-type-' . $search_name,  'class' => 'supersearch-search-type-table'));
            $output .= "</div";
        }
    }
    $output .= drupal_render($form);
    return $output;
}

/*
function search_search_type_submit($form, &$form_state) {
  $weigths = array();
  foreach ($form['results_view_weigths']['type']['#options'] as $name => $value) {
    $index = $name . "-weigth-";
    $weigths[$name] = array();
    foreach ($form_state['values'] as $key => $data) {
      if (strpos($key, $index) === 0) {
        $id = drupal_substr($key, drupal_strlen($index));
        $weigths[$name][$data] = $id;
      }
    }
    ksort($weigths[$name]);
  }

  $weigths['default'] = get_results_page_elems_default_weigths();
  variable_set('results_elems_weigths', $weigths);
}
*/