<?PHP

function supersearch_indexer_settings(&$form_state) {
  $modules = supersearch_indexer_modules_list();
  supersearch_indexer_add_new_modules($modules);
  $modules = supersearch_indexer_modules_list_from_db();

  $form = array();
  $form['hook_settings'] = array('#type' => 'fieldset', '#title' => t('Search Hook Settings & Statistics'));
  $form['hook_settings']['help'] = array('#value' => t('<p>Abbreviations:<br />
                                                           SPH - search_preprocess hook<br />
                                                           UIH - update_index hook'));
  $form['hook_settings']['#theme'] = 'supersearch_indexer_settings_stats_table';
  while ($row = db_fetch_object($modules)) {
    $form['hook_settings']['rows'][$row->id]['data']=array(
      '#type' => 'value',
      '#value' => $row->module_name,
    );

    if ($row->enabled_search_preprocess) {
      $form['hook_settings']['rows'][$row->id]['enabled-search-preprocess-' . $row->id] = array(
        '#type' => 'checkbox',
        '#default_value' => $row->enabled_search_preprocess==1,
      );

      $last_search_preprocess_exec_interval = $row->times_search_preprocess_exec_count ? sprintf("%01.2f sec", $row->last_search_preprocess_exec_interval) :
                                                                                   t('Never successfully ran');

      $avg_search_preprocess_exec_interval = $row->times_search_preprocess_exec_count ? sprintf("%01.2f sec", $row->total_search_preprocess_exec_interval/$row->times_search_preprocess_exec_count) :
                                                                                   t('Never successfully ran');

      $max_search_preprocess_exec_interval = $row->times_search_preprocess_exec_count ? sprintf("%01.2f sec", $row->max_search_preprocess_exec_interval) :
                                                                                   t('Never successfully ran');
    }
    else{
      $form['hook_settings']['rows'][$row->id]['enabled-search-preprocess-' . $row->id] = array(
        '#type' => 'markup',
        '#value' => t('Not Available'),
      );

      $last_search_preprocess_exec_interval = $avg_search_preprocess_exec_interval = $max_search_preprocess_exec_interval = "";
    }

    if (!$row->enabled_update_index) {
      $form['hook_settings']['rows'][$row->id]['enabled-update-index-' . $row->id] = array(
        '#type' => 'markup',
        '#value' => t('Not Available'),
      );
      
      $last_update_index_exec_interval = $avg_update_index_exec_interval = $max_update_index_exec_interval = "";
    }
    else {
      if ($row->module_name == 'node') {
        $form['hook_settings']['rows'][$row->id]['enabled-update-index-' . $row->id] = array(
          '#type' => 'markup',
          '#value' => t('Reimlemented by supersearch'),
        );
        $last_update_index_exec_interval = $avg_update_index_exec_interval = $max_update_index_exec_interval = "";
      }
      else{
        $form['hook_settings']['rows'][$row->id]['enabled-update-index-' . $row->id] = array(
          '#type' => 'checkbox',
          '#default_value' => $row->enabled_update_index==1,
        );

        $last_update_index_exec_interval = $row->times_update_index_exec_count ? sprintf("%01.2f sec", $row->last_update_index_exec_interval) :
                                                                                   t('Never successfully ran');

        $avg_update_index_exec_interval = $row->times_update_index_exec_count ? sprintf("%01.2f sec", $row->total_update_index_exec_interval/$row->times_update_index_exec_count) :
                                                                                   t('Never successfully ran');

        $max_update_index_exec_interval = $row->times_update_index_exec_count ? sprintf("%01.2f sec", $row->max_update_index_exec_interval) :
                                                                                 t('Never successfully ran');
      }
    }

    $form['hook_settings']['rows'][$row->id]['last-search-preprocess-exec-interval'] = array(
        '#type' => 'markup',
        '#value' => $last_search_preprocess_exec_interval,
    );

    $form['hook_settings']['rows'][$row->id]['avg-search-preprocess-exec-interval'] = array(
        '#type' => 'markup',
        '#value' => $avg_search_preprocess_exec_interval,
    );

    $form['hook_settings']['rows'][$row->id]['max-search-preprocess-exec-interval'] = array(
        '#type' => 'markup',
        '#value' => $max_search_preprocess_exec_interval,
    );

    $form['hook_settings']['rows'][$row->id]['last-update-index-exec-interval'] = array(
        '#type' => 'markup',
        '#value' => $last_update_index_exec_interval,
    );

    $form['hook_settings']['rows'][$row->id]['avg-update-index-exec-interval'] = array(
        '#type' => 'markup',
        '#value' => $avg_update_index_exec_interval,
    );

    $form['hook_settings']['rows'][$row->id]['max-update-index-exec-interval'] = array(
        '#type' => 'markup',
        '#value' => $max_update_index_exec_interval,
    );

    $result = db_query("SELECT * FROM {watchdog} WHERE type='searchpreprocess' OR type='updateindex' ORDER BY wid DESC");

    $act = FALSE;
    while ($r = db_fetch_array($result)) {
      $variables = unserialize($r["variables"]);
      if ($variables["@module"] == $row->module_name) {
        $act = TRUE;
        break;
      }
    }

    $form['hook_settings']['rows'][$row->id]['exception_link'] = array(
      '#type' => 'markup',
      '#value' => $act ? l(t('Exceptions'), 'admin/settings/superindexer/exception/'. $row->id) : "",
    );

    $form['hook_settings']['rows'][$row->id]['weight-' . $row->id] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#default_value' => $row->weight,
      '#attributes' => array('class' => 'weight'),
    );
  }

   // Indexing settings:
  $form['indexing_settings'] = array('#type' => 'fieldset', '#title' => t('Indexing settings'), '#collapsible' => TRUE);
  $form['indexing_settings']['info'] = array('#value' => t('<p><em>Changing the settings below will cause the site to be re-indexed. The search index is not cleared but systematically updated to reflect the new settings. Searching will continue to work but new content won\'t be indexed until all existing content has been re-indexed.</em></p><p><em>The default settings should be appropriate for the majority of sites.</em></p>'));
  $form['indexing_settings']['minimum_word_size'] = array('#type' => 'textfield', '#title' => t('Minimum word length to index'), '#default_value' => variable_get('minimum_word_size', 3), '#size' => 5, '#maxlength' => 3, '#description' => t('The number of characters a word has to be to be indexed. A lower setting means better search result ranking, but also a larger database. Each search query must contain at least one keyword that is this size (or longer).'));
  $form['indexing_settings']['overlap_cjk'] = array('#type' => 'checkbox', '#title' => t('Simple CJK handling'), '#default_value' => variable_get('overlap_cjk', TRUE), '#description' => t('Whether to apply a simple Chinese/Japanese/Korean tokenizer based on overlapping sequences. Turn this off if you want to use an external preprocessor for this instead. Does not affect other languages.'));
   
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

      
function supersearch_indexer_settings_submit($form, &$form_state) {
  
// If these settings change, the index needs to be rebuilt.
  if ((variable_get('minimum_word_size', 3) != $form_state['values']['minimum_word_size']) ||
      (variable_get('overlap_cjk', TRUE) != $form_state['values']['overlap_cjk'])) {
    
    variable_set('minimum_word_size' , $form_state['values']['minimum_word_size']);
    variable_set('overlap_cjk' , $form_state['values']['overlap_cjk']);
    
    drupal_set_message(t('The index will be rebuilt.'));
    supersearch_wipe();
  }

  $form['indexing_settings']['minimum_word_size'] = array('#type' => 'textfield', '#title' => t('Minimum word length to index'), '#default_value' => variable_get('minimum_word_size', 3), '#size' => 5, '#maxlength' => 3, '#description' => t('The number of characters a word has to be to be indexed. A lower setting means better search result ranking, but also a larger database. Each search query must contain at least one keyword that is this size (or longer).'));
  $form['indexing_settings']['overlap_cjk'] = array('#type' => 'checkbox', '#title' => t('Simple CJK handling'), '#default_value' => variable_get('overlap_cjk', TRUE), '#description' => t('Whether to apply a simple Chinese/Japanese/Korean tokenizer based on overlapping sequences. Turn this off if you want to use an external preprocessor for this instead. Does not affect other languages.'));
  
  
  $weights = array();

  foreach ($form_state['values'] as $key => $data) {
    if (strpos($key, 'weight-') === 0) {
      $id = drupal_substr($key, drupal_strlen('weight-'));
      $weights[$id] = $form_state['values'][$key];
    }
  }

  $min_weight = min($weights);
  foreach ($weights as $module_id => $value) {
    if ($min_weight)
      $weights[$module_id] -= $min_weight;
    $enabled_string = "";
    if (isset($form_state["values"]["enabled-search-preprocess-" . $module_id]))
      $enabled_string = ", enabled_search_preprocess=" . ($form_state["values"]["enabled-search-preprocess-" . $module_id]==1 ? 1 : 2);
    if (isset($form_state["values"]["enabled-update-index-" . $module_id]))
      $enabled_string .= ", enabled_update_index=" . ($form_state["values"]["enabled-update-index-" . $module_id]==1 ? 1 : 2);

    db_query("UPDATE {search_indexer_enabled_mods_hooks} SET weight=%d" . $enabled_string . " WHERE id=%d", $weights[$module_id], $module_id);
  }
}

function theme_supersearch_indexer_settings_stats_table($form) {

  $header = array(t('Name'), t('SPH'), t('UIH'), t('Last Execution Time<br />(SPH)'), t('Avg. Execution Time<br />(SPH)'), t('Max. Execution Time<br />(SPH)'), t('Last Execution Time<br />(UIH)'), t('Avg. Execution Time<br />(UIH)'), t('Max. Execution Time<br />(UIH)'), t('Exceptions'), t('Weight'));//, t('Max. Execution Time of search_preprocess hook'), t('Output'), t('Exceptions'), t('Weight'));  
  foreach ($form['rows'] as $id => $row) {
    if (isset($row['data'])) {
      $this_row=array();
      $this_row[] = check_plain((string)$row['data']['#value']);
      $this_row[] = drupal_render($row['enabled-search-preprocess-'. $id]);
      $this_row[] = drupal_render($row['enabled-update-index-'. $id]);
      $this_row[] = drupal_render($row['last-search-preprocess-exec-interval']);
      $this_row[] = drupal_render($row['avg-search-preprocess-exec-interval']);
      $this_row[] = drupal_render($row['max-search-preprocess-exec-interval']);
      $this_row[] = drupal_render($row['last-update-index-exec-interval']);
      $this_row[] = drupal_render($row['avg-update-index-exec-interval']);
      $this_row[] = drupal_render($row['max-update-index-exec-interval']);
      $this_row[] = drupal_render($row['exception_link']);
      $this_row[] = drupal_render($row['weight-'. $id]);
      $table_rows[] = array('data' => $this_row, 'class' => 'draggable');
      unset($form['rows'][$id]);
    }
  }

  drupal_add_tabledrag('supersearch_indexer-table', 'order', 'sibling', 'weight');
  $output .= theme('table', $header, $table_rows, array('id' => 'supersearch_indexer-table'));
  $output .= drupal_render($form);
  return $output;
}

