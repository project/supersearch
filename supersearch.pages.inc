<?php

/**
 * @file
 * User page callbacks for the supersearch module.
 */

/**
 * Menu callback; presents the search form and/or search results.
 */
function supersearch_view($type = 'node') {
// Search form submits with POST but redirects to GET. This way we can keep
// the search query URL clean as a whistle:
// search/type/keyword+keyword
//include_once 'search_compatibility.inc';
//include_once 'search_compatibility.pages.inc';
    if (!isset($_POST['form_id'])) {
        if ($type == '') {
        // Note: search/node can not be a default tab because it would take on the
        // path of its parent (search). It would prevent remembering keywords when
        // switching tabs. This is why we drupal_goto to it from the parent instead.
            drupal_goto(_supersearch_result_fragment('/') . 'node');
        }

        $keys = supersearch_get_keys();

    /*Banned terms cannot be searched for (they should return an empty result set).*/

        if (variable_get('ban_term_not_search', TRUE)) {
            include 'supersearch.banned-term-management.inc';
            $banned_keys = _supersearch_return_banned_terms();
            foreach ($banned_keys as $_banned_keys) {
                if($keys==$_banned_keys["terms"]) {
                    return supersearch_prepare_results_output(array(), $keys, $type);
                }
            }
        }

        if (trim($keys)) {
            $history_term = array('type' => $type, 'keywords' => $keys, 'url' => str_replace('/?q=', '', request_uri()));
            $history = unserialize(base64_decode($_COOKIE['supersearch']));
            if (!is_array($history)) $history = array();
            $history = array_diff_assoc($history, $history_term);
            $history[$keys] = $history_term;
            $max_history_terms = variable_get('search_history_count', 5);

            if (count($history) > $max_history_terms) $history = array_slice($history, count($history) - $max_history_terms, $max_history_terms, TRUE);

            setcookie('supersearch', base64_encode(serialize($history)));

        }

        // Only perform search if there is non-whitespace search term:
        $results = '';
        if (trim($keys)) {
        // Log the search keys:
            watchdog('search', '%keys (@type).', array('%keys' => $keys, '@type' => module_invoke($type, 'search', 'name')), WATCHDOG_NOTICE, l(t('results'), _supersearch_result_fragment('/'). $type .'/'. $keys));
            $results = supersearch_data($keys, $type);
        }

        $output = supersearch_prepare_results_output($results, $keys, $type);
        return $output;
    }

    return drupal_get_form('search_form', NULL, empty($keys) ? '' : $keys, $type);
}


function supersearch_searchpage_items($type = 'node') {
  if ($type == 'node') {
    $keywords = array_keys(supersearch_get_top_terms(TOP_RESULTS_NORMAL, variable_get('top_terms_count_topX', 10), variable_get('top_terms_sort_type', TOP_RESULTS_SORT_DESC_RANK), variable_get('search_terms_list_region', TOP_RESULTS_SEO_REGION_END), !variable_get('ban_term_not_visible_TopX', TRUE)));
    $output = '<div id="topX">';
    foreach ($keywords as $_keywords) {
      $output .= l($_keywords, _supersearch_result_fragment('/') . 'node/' . $_keywords) . ' ';
    }

    //debug
    if (function_exists('supersearch_TopX_URL')){
      $output .= l('More...', supersearch_TopX_URL()) . "</div>";
    }

    $return = db_query_range("SELECT nid FROM {supersearch_help_nodes} WHERE terms LIKE '%s'", arg(2), 0, 1);
    //thn INNER JOIN {topresults_data} td ON thn.id=td.id WHERE keywords='%s'", arg(2), 0, 1);
    if ($return) {
      $row = db_fetch_object($return);
      if ($row) {
        $node = node_view(node_load($row->nid));
      }
      else {
        $node = '';
      }
    }

    return array('topX' => $output, 'helper_node' => $node);
  }

  return array();
}



function supersearch_prepare_results_output($results, $keys, $type) {
//include_once 'search_compatibility.inc';
//include_once 'search_compatibility.pages.inc';

    drupal_set_header('cache-control: no-cache');

    if (!variable_get('sponsor_links_disabled', FALSE))
        drupal_set_content('footer', 'Search results brought to you by ' . l('63reasons', 'http://www.63reasons.com/') . ' in sponsorship with ' . l('Cliff\'s List', 'http://www.cliffslist.com/'));

    $elems = array();
    $elems['search_form'] = drupal_get_form('search_form', NULL, $keys, $type);

    if ($results !== '') {
        if (count($results)) {
            $elems['search_results_title'] = t('<h2 class="title">Search results</h2>');
            $elems['search_results'] = $results;
        }
        else {
            $elems['search_results_title'] = t('<h2 class="title">Your search yielded no results</h2>');
            $elems['search_results'] = supersearch_help('supersearch#noresults', drupal_help_arg());
        }
    }

    $add_elems = module_invoke_all('searchpage_items', $type);
    $elems = array_merge($elems, $add_elems);

    $results_elems_weigths = get_search_results_elem_weigths($type);
    if (empty($results_elems_weigths)) {
      $results_elems_weigths = supersearch_register_search_results_elem_weigths($type);
    }

    $output = '';

    if ($results_elems_weigths)
        foreach ($results_elems_weigths as $elem_name) {
            if (isset($elems[$elem_name]))
                $output .= $elems[$elem_name];
        }

    $template = NULL;
    $row = db_fetch_object(db_query_range("SELECT template_name FROM {search_term_template_assoc} WHERE keyword='%s'", $keys, 0, 1));
    if ($row)
        $template = $row->template_name;
    $history = get_search_history(0, variable_get('search_history_asc', 1)==0 ? "ASC" : "DESC");
    if (!empty($history))
        $history_output = !empty($history) ? theme('supersearch_prepare_history', $history) : "";
    if (strlen(trim($history_output)) == 0) $history_output = '<i>' . t('none') . '</i>';
    $output = str_replace("[%KEYWORD%]", $keys, theme('supersearch_prepare_results_output', $output, $template));
    $output = str_replace("[%HISTORY%]", $history_output, $output);
    return $output;
}

function template_preprocess_supersearch_prepare_results_output(&$variables) {
    if ($variables['alt_template'])
        $variables['template_files'][] = "search-by-keyword-" . $variables['alt_template'];
}

function theme_supersearch_prepare_history($history) {
    $types = array();
    foreach (module_implements('search') as $name) {
        $types[module_invoke($name, 'search', 'name', TRUE)] = $name;
    }

    $output='<ul>';
    foreach ($history as $item) {
        $output .= "<li>" . l($item["keywords"], $item['url'], array('absolute' => TRUE)) . "</li>";
    }
    return $output . "</ul>";
}



/**
 * Process variables for search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $type
 *
 * @see search-result.tpl.php
 */

if (!module_exists('search')) include_once('search_compatibility.pages.inc');