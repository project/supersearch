<?PHP

// Public-facing API as per function search_query_extract($keys, $option) {return supersearch_query_extract($keys, $option);}

//function do_search($keywords, $type, $join1 = '', $where1 = '1 = 1', $arguments1 = array(), $columns2 = 'i.relevance AS score', $join2 = '', $arguments2 = array(), $sort_parameters = 'ORDER BY score DESC') {return do_supersearch($keywords, $type, $join1, $where1, $arguments1, $columns2, $join2, $arguments2, $sort_parameters);}
function search_box(&$form_state, $form_id) {return supersearch_box(&$form_state, $form_id);}
function search_data($keys = NULL, $type = 'node') {
    return supersearch_data($keys, $type);}
function search_excerpt($keys, $text) {return supersearch_excerpt($keys, $text);}

function search_index($sid, $type, $text) {return supersearch_index($sid, $type, $text);}
//function search_form(&$form_state, $action = '', $keys = '', $type = NULL, $prompt = NULL) {return supersearch_form(&$form_state, $action, $keys, $type, $prompt );}

// Functions used in code modules

function search_wipe($sid = NULL, $type = NULL, $reindex = FALSE) {return supersearch_wipe($sid, $type, $reindex);}
function search_query_insert($keys, $option, $value = '') {return supersearch_query_insert($keys, $option, $value);}
function search_query_extract($keys, $option) {return supersearch_query_extract($keys, $option);}


/**
 * Process variables for search-theme-form.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $form
 *
 * @see search-theme-form.tpl.php
 */
function template_preprocess_search_theme_form(&$variables) {
  $variables['search'] = array();
  $hidden = array();
  // Provide variables named after form keys so themers can print each element independently.
  foreach (element_children($variables['form']) as $key) {
    $type = $variables['form'][$key]['#type'];
    if ($type == 'hidden' || $type == 'token') {
      $hidden[] = drupal_render($variables['form'][$key]);
    }
    else {
      $variables['search'][$key] = drupal_render($variables['form'][$key]);
    }
  }
  // Hidden form elements have no value to themers. No need for separation.
  $variables['search']['hidden'] = implode($hidden);
  // Collect all form elements to make it easier to print the whole form.
  $variables['search_form'] = implode($variables['search']);
}

/**
 * Process variables for search-block-form.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $form
 *
 * @see search-block-form.tpl.php
 */
function template_preprocess_search_block_form(&$variables) {
  $variables['search'] = array();
  $hidden = array();
  // Provide variables named after form keys so themers can print each element independently.
  foreach (element_children($variables['form']) as $key) {
    $type = $variables['form'][$key]['#type'];
    if ($type == 'hidden' || $type == 'token') {
      $hidden[] = drupal_render($variables['form'][$key]);
    }
    else {
      $variables['search'][$key] = drupal_render($variables['form'][$key]);
    }
  }
  // Hidden form elements have no value to themers. No need for separation.
  $variables['search']['hidden'] = implode($hidden);
  // Collect all form elements to make it easier to print the whole form.
  $variables['search_form'] = implode($variables['search']);
}
