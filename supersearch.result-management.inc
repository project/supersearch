<?php

Define('SUPERSEARCH_RESULTS_CACHE_TABLE', 'cache_search_results');

function supersearch_results_cache_clear() {
    cache_clear_all('*', SUPERSEARCH_RESULTS_CACHE_TABLE, TRUE);
}

function supersearch_flush_caches()
{
 return array(SUPERSEARCH_RESULTS_CACHE_TABLE);
}

/**
 * Wipes a part of or the entire search index.
 *
 * @param $sid
 *  (optional) The SID of the item to wipe. If specified, $type must be passed
 *  too.
 * @param $type
 *  (optional) The type of item to wipe.
 */
function supersearch_wipe($sid = NULL, $type = NULL, $reindex = FALSE) {
    if ($type == NULL && $sid == NULL) {
        module_invoke_all('search', 'reset');
    }
    else {
        db_query("DELETE FROM {search_dataset} WHERE sid = %d AND type = '%s'", $sid, $type);
        db_query("DELETE FROM {search_index} WHERE sid = %d AND type = '%s'", $sid, $type);
        // Don't remove links if re-indexing.
        if (!$reindex) {
            db_query("DELETE FROM {search_node_links} WHERE sid = %d AND type = '%s'", $sid, $type);
        }
    }
    supersearch_results_cache_clear();
}

/**
 * Extract a module-specific search option from a search query. e.g. 'type:book'
 */
function supersearch_query_extract($keys, $option) {
    if (preg_match('/(^| )'. $option .':([^ ]*)( |$)/i', $keys, $matches)) {
        return $matches[2];
    }
}

/**
 * Return a query with the given module-specific search option inserted in.
 * e.g. 'type:book'.
 */
function supersearch_query_insert($keys, $option, $value = '') {
    if (supersearch_query_extract($keys, $option)) {
        $keys = trim(preg_replace('/(^| )'. $option .':[^ ]*/i', '', $keys));
    }
    if ($value != '') {
        $keys .= ' '. $option .':'. $value;
    }
    return $keys;
}

/**
 * Parse a search query into SQL conditions.
 *
 * We build two queries that matches the dataset bodies. @See do_supersearch for
 * more about these.
 *
 * @param $text
 *   The search keys.
 * @return
 *   A list of six elements.
 *    * A series of statements AND'd together which will be used to provide all
 *      possible matches.
 *    * Arguments for this query part.
 *    * A series of exact word matches OR'd together.
 *    * Arguments for this query part.
 *    * A bool indicating whether this is a simple query or not. Negative
 *      terms, presence of both AND / OR make this FALSE.
 *    * A bool indicating the presence of a lowercase or. Maybe the user
 *      wanted to use OR.
 */

function supersearch_parse_query($text) {
    $keys = array('positive' => array(), 'negative' => array());

    // Tokenize query string
    preg_match_all('/ (-?)("[^"]+"|[^" ]+)/i', ' '. $text, $matches, PREG_SET_ORDER);

    if (count($matches) < 1) {
        return NULL;
    }

    // Classify tokens
    $or = FALSE;
    $warning = '';
    $simple = TRUE;
    foreach ($matches as $match) {
        $phrase = FALSE;
        // Strip off phrase quotes
        if ($match[2]{0} == '"') {
            $match[2] = substr($match[2], 1, -1);
            $phrase = TRUE;
            $simple = FALSE;
        }
        // Simplify keyword according to indexing rules and external preprocessors
        $words = supersearch_simplify($match[2]);
        // Re-explode in case simplification added more words, except when matching a phrase
        $words = $phrase ? array($words) : preg_split('/ /', $words, -1, PREG_SPLIT_NO_EMPTY);
        // Negative matches
        if ($match[1] == '-') {
            $keys['negative'] = array_merge($keys['negative'], $words);
        }
        // OR operator: instead of a single keyword, we store an array of all
        // OR'd keywords.
        elseif ($match[2] == 'OR' && count($keys['positive'])) {
            $last = array_pop($keys['positive']);
            // Starting a new OR?
            if (!is_array($last)) {
                $last = array($last);
            }
            $keys['positive'][] = $last;
            $or = TRUE;
            continue;
        }
        // AND operator: implied, so just ignore it
        elseif ($match[2] == 'AND' || $match[2] == 'and') {
            $warning = $match[2];
            continue;
        }

        // Plain keyword
        else {
            if ($match[2] == 'or') {
                $warning = $match[2];
            }
            if ($or) {
            // Add to last element (which is an array)
                $keys['positive'][count($keys['positive']) - 1] = array_merge($keys['positive'][count($keys['positive']) - 1], $words);
            }
            else {
                $keys['positive'] = array_merge($keys['positive'], $words);
            }
        }
        $or = FALSE;
    }

    // Convert keywords into SQL statements.
    $query = array();
    $query2 = array();
    $arguments = array();
    $arguments2 = array();
    $matches = 0;
    $simple_and = FALSE;
    $simple_or = FALSE;
    // Positive matches
    foreach ($keys['positive'] as $key) {
    // Group of ORed terms
        if (is_array($key) && count($key)) {
            $simple_or = TRUE;
            $queryor = array();
            $any = FALSE;
            foreach ($key as $or) {
                list($q, $num_new_scores) = _supersearch_parse_query($or, $arguments2);
                $any |= $num_new_scores;
                if ($q) {
                    $queryor[] = $q;
                    $arguments[] = $or;
                }
            }
            if (count($queryor)) {
                $query[] = '('. implode(' OR ', $queryor) .')';
                // A group of OR keywords only needs to match once
                $matches += ($any > 0);
            }
        }
        // Single ANDed term
        else {
            $simple_and = TRUE;
            list($q, $num_new_scores, $num_valid_words) = _supersearch_parse_query($key, $arguments2);
            if ($q) {
                $query[] = $q;
                $arguments[] = $key;
                if (!$num_valid_words) {
                    $simple = FALSE;
                }
                // Each AND keyword needs to match at least once
                $matches += $num_new_scores;
            }
        }
    }
    if ($simple_and && $simple_or) {
        $simple = FALSE;
    }
    // Negative matches
    foreach ($keys['negative'] as $key) {
        list($q) = _supersearch_parse_query($key, $arguments2, TRUE);
        if ($q) {
            $query[] = $q;
            $arguments[] = $key;
            $simple = FALSE;
        }
    }
    $query = implode(' AND ', $query);

    // Build word-index conditions for the first pass
    $query2 = substr(str_repeat("i.word = '%s' OR ", count($arguments2)), 0, -4);

    return array($query, $arguments, $query2, $arguments2, $matches, $simple, $warning);
}

/**
 * Helper function for search_parse_query();
 */
function _supersearch_parse_query(&$word, &$scores, $not = FALSE) {
    $num_new_scores = 0;
    $num_valid_words = 0;
    // Determine the scorewords of this word/phrase
    if (!$not) {
        $split = explode(' ', $word);
        foreach ($split as $s) {
            $num = is_numeric($s);
            if ($num || drupal_strlen($s) >= variable_get('minimum_word_size', 3)) {
                $s = $num ? ((int)ltrim($s, '-0')) : $s;
                if (!isset($scores[$s])) {
                    $scores[$s] = $s;
                    $num_new_scores++;
                }
                $num_valid_words++;
            }
        }
    }
    // Return matching snippet and number of added words
    return array("d.data ". ($not ? 'NOT ' : '') ."LIKE '%% %s %%'", $num_new_scores, $num_valid_words);
}

/**
 * Do a query on the full-text search index for a word or words.
 *
 * This function is normally only called by each module that support the
 * indexed search (and thus, implements hook_update_index()).
 *
 * Results are retrieved in two logical passes. However, the two passes are
 * joined together into a single query.  And in the case of most simple
 * queries the second pass is not even used.
 *
 * The first pass selects a set of all possible matches, which has the benefit
 * of also providing the exact result set for simple "AND" or "OR" searches.
 *
 * The second portion of the query further refines this set by verifying
 * advanced text conditions (such negative or phrase matches)
 *
 * @param $keywords
 *   A search string as entered by the user.
 *
 * @param $type
 *   A string identifying the calling module.
 *
 * @param $join1
 *   (optional) Inserted into the JOIN part of the first SQL query.
 *   For example "INNER JOIN {node} n ON n.nid = i.sid".
 *
 * @param $where1
 *   (optional) Inserted into the WHERE part of the first SQL query.
 *   For example "(n.status > %d)".
 *
 * @param $arguments1
 *   (optional) Extra SQL arguments belonging to the first query.
 *
 * @param $columns2
 *   (optional) Inserted into the SELECT pat of the second query. Must contain
 *   a column selected as 'score'.
 *   defaults to 'i.relevance AS score'
 *
 * @param $join2
 *   (optional) Inserted into the JOIN par of the second SQL query.
 *   For example "INNER JOIN {node_comment_statistics} n ON n.nid = i.sid"
 *
 * @param $arguments2
 *   (optional) Extra SQL arguments belonging to the second query parameter.
 *
 * @param $sort_parameters
 *   (optional) SQL arguments for sorting the final results.
 *              Default: 'ORDER BY score DESC'
 *
 * @return
 *   An array of SIDs for the search results.
 *
 * @ingroup search
 */

function do_search($keywords, $type, $join1 = '', $where1 = '1 = 1', $arguments1 = array(), $columns2 = 'i.relevance AS score', $join2 = '', $arguments2 = array(), $sort_parameters = 'ORDER BY score DESC') {

//function do_supersearch($keywords, $type, $join1 = '', $where1 = '1 = 1', $arguments1 = array(), $columns2 = 'i.relevance AS score', $join2 = '', $arguments2 = array(), $sort_parameters = 'ORDER BY score DESC') {
    $query = supersearch_parse_query($keywords);

    if ($query[2] == '') {
        form_set_error('keys', t('You must include at least one positive keyword with @count characters or more.', array('@count' => variable_get('minimum_word_size', 3))));
    }
    if ($query[6]) {
        if ($query[6] == 'or') {
            drupal_set_message(t('Search for either of the two terms with uppercase <strong>OR</strong>. For example, <strong>cats OR dogs</strong>.'));
        }
    }
    if ($query === NULL || $query[0] == '' || $query[2] == '') {
        return array();
    }

    // Build query for keyword normalization.
    $conditions = "$where1 AND ($query[2]) AND i.type = '%s'";
    $arguments1 = array_merge($arguments1, $query[3], array($type));
    $join = "INNER JOIN {search_total} t ON i.word = t.word $join1";
    if (!$query[5]) {
        $conditions .= " AND ($query[0])";
        $arguments1 = array_merge($arguments1, $query[1]);
        $join .= " INNER JOIN {search_dataset} d ON i.sid = d.sid AND i.type = d.type";
    }

    // Calculate maximum keyword relevance, to normalize it.
    $select = "SELECT SUM(i.score * t.count) AS score FROM {search_index} i $join WHERE $conditions GROUP BY i.type, i.sid HAVING COUNT(*) >= %d ORDER BY score DESC";
    $arguments = array_merge($arguments1, array($query[4]));
    $normalize = db_result(db_query_range($select, $arguments, 0, 1));
    if (!$normalize) {
        return array();
    }
    $columns2 = str_replace('i.relevance', '('. (1.0 / $normalize) .' * SUM(i.score * t.count))', $columns2);

    // Build query to retrieve results.
    $select = "SELECT i.type, i.sid, $columns2 FROM {search_index} i $join $join2 WHERE $conditions GROUP BY i.type, i.sid HAVING COUNT(*) >= %d";
    $count_select =  "SELECT COUNT(*) FROM ($select) n1";
    $arguments = array_merge($arguments2, $arguments1, array($query[4]));

    // Do actual search query
    $result = pager_query("$select $sort_parameters", 10, 0, $count_select, $arguments);
    $results = array();
    while ($item = db_fetch_object($result)) {
        $results[] = $item;
    }
    return $results;
}


/**
 * Helper function for grabbing search keys.
 */
function supersearch_get_keys() {
    static $return;
    if (!isset($return)) {
    // Extract keys as remainder of path
    // Note: support old GET format of searches for existing links.
        $path = explode('/', $_GET['q'], 3);
        $keys = empty($_REQUEST['keys']) ? '' : $_REQUEST['keys'];
        $result = count($path) == 3 ? $path[2] : $keys;
        $return = supersearch_decode_url($result);
    }
    return $return;
}

function supersearch_TopX_URL() {
    return _supersearch_result_fragment('/') . 'node/' . 'top-' . variable_get('top_terms_count_topX', 10) . '-searched-terms';
}

/**
 * Change a node's changed timestamp to 'now' to force reindexing.
 *
 * @param $nid
 *   The nid of the node that needs reindexing.
 */
function supersearch_touch_node($nid) {
    db_query("UPDATE {search_dataset} SET reindex = %d WHERE sid = %d AND type = 'node'", time(), $nid);
    supersearch_results_cache_clear();
}

/**
 * Implementation of hook_nodeapi().
 */
function supersearch_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
    switch ($op) {
    // Transplant links to a node into the target node.
        case 'update index':
            $result = db_query("SELECT caption FROM {search_node_links} WHERE nid = %d", $node->nid);
            $output = array();
            while ($link = db_fetch_object($result)) {
                $output[] = $link->caption;
            }
            if (count($output)) {
                return '<a>('. implode(', ', $output) .')</a>';
            }
        // Reindex the node when it is updated.  The node is automatically indexed
        // when it is added, simply by being added to the node table.
        case 'update':
            supersearch_touch_node($node->nid);
            break;
    }
}

/**
 * Implementation of hook_comment().
 */
function supersearch_comment($a1, $op) {
    switch ($op) {
    // Reindex the node when comments are added or changed
        case 'insert': supersearch_results_cache_clear();
        case 'update':
            supersearch_touch_node(is_array($a1) ? $a1['nid'] : $a1->nid);
            break;
        case 'delete': supersearch_results_cache_clear();
        case 'delete revision': supersearch_results_cache_clear();
        case 'publish':
        case 'unpublish':
            supersearch_touch_node(is_array($a1) ? $a1['nid'] : $a1->nid);
            break;
    }
}



/**
 * Returns snippets from a piece of text, with certain keywords highlighted.
 * Used for formatting search results.
 *
 * @param $keys
 *   A string containing a search query.
 *
 * @param $text
 *   The text to extract fragments from.
 *
 * @return
 *   A string containing HTML for the excerpt.
 */
function supersearch_excerpt($keys, $text) {
// We highlight around non-indexable or CJK characters.
    $boundary = '(?:(?<=['. PREG_CLASS_SEARCH_EXCLUDE . PREG_CLASS_CJK .'])|(?=['. PREG_CLASS_SEARCH_EXCLUDE . PREG_CLASS_CJK .']))';

    // Extract positive keywords and phrases
    preg_match_all('/ ("([^"]+)"|(?!OR)([^" ]+))/', ' '. $keys, $matches);
    $keys = array_merge($matches[2], $matches[3]);

    // Prepare text
    $text = ' '. strip_tags(str_replace(array('<', '>'), array(' <', '> '), $text)) .' ';
    array_walk($keys, '_supersearch_excerpt_replace');
    $workkeys = $keys;

    // Extract a fragment per keyword for at most 4 keywords.
    // First we collect ranges of text around each keyword, starting/ending
    // at spaces.
    // If the sum of all fragments is too short, we look for second occurrences.
    $ranges = array();
    $included = array();
    $length = 0;
    while ($length < 256 && count($workkeys)) {
        foreach ($workkeys as $k => $key) {
            if (strlen($key) == 0) {
                unset($workkeys[$k]);
                unset($keys[$k]);
                continue;
            }
            if ($length >= 256) {
                break;
            }
            // Remember occurrence of key so we can skip over it if more occurrences
            // are desired.
            if (!isset($included[$key])) {
                $included[$key] = 0;
            }
            // Locate a keyword (position $p), then locate a space in front (position
            // $q) and behind it (position $s)
            if (preg_match('/'. $boundary . $key . $boundary .'/iu', $text, $match, PREG_OFFSET_CAPTURE, $included[$key])) {
                $p = $match[0][1];
                if (($q = strpos($text, ' ', max(0, $p - 60))) !== FALSE) {
                    $end = substr($text, $p, 80);
                    if (($s = strrpos($end, ' ')) !== FALSE) {
                        $ranges[$q] = $p + $s;
                        $length += $p + $s - $q;
                        $included[$key] = $p + 1;
                    }
                    else {
                        unset($workkeys[$k]);
                    }
                }
                else {
                    unset($workkeys[$k]);
                }
            }
            else {
                unset($workkeys[$k]);
            }
        }
    }

    // If we didn't find anything, return the beginning.
    if (count($ranges) == 0) {
        return truncate_utf8($text, 256) .' ...';
    }

    // Sort the text ranges by starting position.
    ksort($ranges);

    // Now we collapse overlapping text ranges into one. The sorting makes it O(n).
    $newranges = array();
    foreach ($ranges as $from2 => $to2) {
        if (!isset($from1)) {
            $from1 = $from2;
            $to1 = $to2;
            continue;
        }
        if ($from2 <= $to1) {
            $to1 = max($to1, $to2);
        }
        else {
            $newranges[$from1] = $to1;
            $from1 = $from2;
            $to1 = $to2;
        }
    }
    $newranges[$from1] = $to1;

    // Fetch text
    $out = array();
    foreach ($newranges as $from => $to) {
        $out[] = substr($text, $from, $to - $from);
    }
    $text = (isset($newranges[0]) ? '' : '... ') . implode(' ... ', $out) .' ...';

    // Highlight keywords. Must be done at once to prevent conflicts ('strong' and '<strong>').
    $text = preg_replace('/'. $boundary .'('. implode('|', $keys) .')'. $boundary .'/iu', '<strong>\0</strong>', $text);
    return $text;
}

/**
 * @} End of "defgroup search".
 */

/**
 * Helper function for array_walk in search_except.
 */
function _supersearch_excerpt_replace(&$text) {
    $text = preg_quote($text, '/');
}


function supersearch_topX_page() {
    $keywords = array_keys(supersearch_get_top_terms(TOP_RESULTS_NORMAL, -1, TOP_RESULTS_SORT_ALPHA, NULL, !variable_get('ban_term_not_visible_TopX', TRUE)));
    $output = "<div id='topX_page'><ul>";
    foreach ($keywords as $_keywords) {
        $output .= "<li>" . l($_keywords, _supersearch_result_fragment('/') . 'node/' . $_keywords) . "</li>";
    }
    return $output . "</ul></div>";
}


/**
 * Perform a standard search on the given keys, and return the formatted results.
 */


function supersearch_data($keys = NULL, $type = 'node') {
    global $user;

    if (isset($keys)) {
        if (module_hook($type, 'search')) {

            if ($type == 'node') {
                $roles = array_keys($user->roles);
                sort($roles);
                $cid .= $type . "-";
                $cid .= implode("-", $roles);
                $cid .= "-(" . $keys . ")";
                $cache_data = cache_get($cid, SUPERSEARCH_RESULTS_CACHE_TABLE);
                if ($cache_data) $results = $cache_data->data;
                else {
                    $results = module_invoke('node', 'search', 'search', $keys);
                    foreach($results as $key => $node)
                    {
                        $results[$key]['node']->body = NULL;
                        $results[$key]['node']->log = NULL;
                    }
                    cache_set($cid, $results, SUPERSEARCH_RESULTS_CACHE_TABLE);
                }
            }
            else $results = module_invoke($type, 'search', 'search', $keys);
            if (isset($results) && is_array($results) && count($results)) {
                if (module_hook($type, 'search_page')) {
                    return module_invoke($type, 'search_page', $results);
                }
                else {
                    return theme('search_results', $results, $type);
                }
            }
        }
    }
}


function get_search_history($count = 0, $type = 'ASC') {

$history = unserialize(base64_decode($_COOKIE['supersearch']));
if (!is_array($history)) $history = array();
if ($type != 'ASC') $history = array_reverse ($history);
return $history;

}
