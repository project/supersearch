<?php

include_once('supersearch.result-management.inc');

/**
 * @file
 * Enables site-wide keyword searching.
 */

/**
 * Matches Unicode character classes to exclude from the search index.
 *
 * See: http://www.unicode.org/Public/UNIDATA/UCD.html#General_Category_Values
 *
 * The index only contains the following character classes:
 * Lu     Letter, Uppercase
 * Ll     Letter, Lowercase
 * Lt     Letter, Titlecase
 * Lo     Letter, Other
 * Nd     Number, Decimal Digit
 * No     Number, Other
 */
define('PREG_CLASS_SEARCH_EXCLUDE',
    '\x{0}-\x{2f}\x{3a}-\x{40}\x{5b}-\x{60}\x{7b}-\x{bf}\x{d7}\x{f7}\x{2b0}-'.
    '\x{385}\x{387}\x{3f6}\x{482}-\x{489}\x{559}-\x{55f}\x{589}-\x{5c7}\x{5f3}-'.
    '\x{61f}\x{640}\x{64b}-\x{65e}\x{66a}-\x{66d}\x{670}\x{6d4}\x{6d6}-\x{6ed}'.
    '\x{6fd}\x{6fe}\x{700}-\x{70f}\x{711}\x{730}-\x{74a}\x{7a6}-\x{7b0}\x{901}-'.
    '\x{903}\x{93c}\x{93e}-\x{94d}\x{951}-\x{954}\x{962}-\x{965}\x{970}\x{981}-'.
    '\x{983}\x{9bc}\x{9be}-\x{9cd}\x{9d7}\x{9e2}\x{9e3}\x{9f2}-\x{a03}\x{a3c}-'.
    '\x{a4d}\x{a70}\x{a71}\x{a81}-\x{a83}\x{abc}\x{abe}-\x{acd}\x{ae2}\x{ae3}'.
    '\x{af1}-\x{b03}\x{b3c}\x{b3e}-\x{b57}\x{b70}\x{b82}\x{bbe}-\x{bd7}\x{bf0}-'.
    '\x{c03}\x{c3e}-\x{c56}\x{c82}\x{c83}\x{cbc}\x{cbe}-\x{cd6}\x{d02}\x{d03}'.
    '\x{d3e}-\x{d57}\x{d82}\x{d83}\x{dca}-\x{df4}\x{e31}\x{e34}-\x{e3f}\x{e46}-'.
    '\x{e4f}\x{e5a}\x{e5b}\x{eb1}\x{eb4}-\x{ebc}\x{ec6}-\x{ecd}\x{f01}-\x{f1f}'.
    '\x{f2a}-\x{f3f}\x{f71}-\x{f87}\x{f90}-\x{fd1}\x{102c}-\x{1039}\x{104a}-'.
    '\x{104f}\x{1056}-\x{1059}\x{10fb}\x{10fc}\x{135f}-\x{137c}\x{1390}-\x{1399}'.
    '\x{166d}\x{166e}\x{1680}\x{169b}\x{169c}\x{16eb}-\x{16f0}\x{1712}-\x{1714}'.
    '\x{1732}-\x{1736}\x{1752}\x{1753}\x{1772}\x{1773}\x{17b4}-\x{17db}\x{17dd}'.
    '\x{17f0}-\x{180e}\x{1843}\x{18a9}\x{1920}-\x{1945}\x{19b0}-\x{19c0}\x{19c8}'.
    '\x{19c9}\x{19de}-\x{19ff}\x{1a17}-\x{1a1f}\x{1d2c}-\x{1d61}\x{1d78}\x{1d9b}-'.
    '\x{1dc3}\x{1fbd}\x{1fbf}-\x{1fc1}\x{1fcd}-\x{1fcf}\x{1fdd}-\x{1fdf}\x{1fed}-'.
    '\x{1fef}\x{1ffd}-\x{2070}\x{2074}-\x{207e}\x{2080}-\x{2101}\x{2103}-\x{2106}'.
    '\x{2108}\x{2109}\x{2114}\x{2116}-\x{2118}\x{211e}-\x{2123}\x{2125}\x{2127}'.
    '\x{2129}\x{212e}\x{2132}\x{213a}\x{213b}\x{2140}-\x{2144}\x{214a}-\x{2b13}'.
    '\x{2ce5}-\x{2cff}\x{2d6f}\x{2e00}-\x{3005}\x{3007}-\x{303b}\x{303d}-\x{303f}'.
    '\x{3099}-\x{309e}\x{30a0}\x{30fb}-\x{30fe}\x{3190}-\x{319f}\x{31c0}-\x{31cf}'.
    '\x{3200}-\x{33ff}\x{4dc0}-\x{4dff}\x{a015}\x{a490}-\x{a716}\x{a802}\x{a806}'.
    '\x{a80b}\x{a823}-\x{a82b}\x{d800}-\x{f8ff}\x{fb1e}\x{fb29}\x{fd3e}\x{fd3f}'.
    '\x{fdfc}-\x{fe6b}\x{feff}-\x{ff0f}\x{ff1a}-\x{ff20}\x{ff3b}-\x{ff40}\x{ff5b}-'.
    '\x{ff65}\x{ff70}\x{ff9e}\x{ff9f}\x{ffe0}-\x{fffd}');

/**
 * Matches all 'N' Unicode character classes (numbers)
 */
define('PREG_CLASS_NUMBERS',
    '\x{30}-\x{39}\x{b2}\x{b3}\x{b9}\x{bc}-\x{be}\x{660}-\x{669}\x{6f0}-\x{6f9}'.
    '\x{966}-\x{96f}\x{9e6}-\x{9ef}\x{9f4}-\x{9f9}\x{a66}-\x{a6f}\x{ae6}-\x{aef}'.
    '\x{b66}-\x{b6f}\x{be7}-\x{bf2}\x{c66}-\x{c6f}\x{ce6}-\x{cef}\x{d66}-\x{d6f}'.
    '\x{e50}-\x{e59}\x{ed0}-\x{ed9}\x{f20}-\x{f33}\x{1040}-\x{1049}\x{1369}-'.
    '\x{137c}\x{16ee}-\x{16f0}\x{17e0}-\x{17e9}\x{17f0}-\x{17f9}\x{1810}-\x{1819}'.
    '\x{1946}-\x{194f}\x{2070}\x{2074}-\x{2079}\x{2080}-\x{2089}\x{2153}-\x{2183}'.
    '\x{2460}-\x{249b}\x{24ea}-\x{24ff}\x{2776}-\x{2793}\x{3007}\x{3021}-\x{3029}'.
    '\x{3038}-\x{303a}\x{3192}-\x{3195}\x{3220}-\x{3229}\x{3251}-\x{325f}\x{3280}-'.
    '\x{3289}\x{32b1}-\x{32bf}\x{ff10}-\x{ff19}');

/**
 * Matches all 'P' Unicode character classes (punctuation)
 */
define('PREG_CLASS_PUNCTUATION',
    '\x{21}-\x{23}\x{25}-\x{2a}\x{2c}-\x{2f}\x{3a}\x{3b}\x{3f}\x{40}\x{5b}-\x{5d}'.
    '\x{5f}\x{7b}\x{7d}\x{a1}\x{ab}\x{b7}\x{bb}\x{bf}\x{37e}\x{387}\x{55a}-\x{55f}'.
    '\x{589}\x{58a}\x{5be}\x{5c0}\x{5c3}\x{5f3}\x{5f4}\x{60c}\x{60d}\x{61b}\x{61f}'.
    '\x{66a}-\x{66d}\x{6d4}\x{700}-\x{70d}\x{964}\x{965}\x{970}\x{df4}\x{e4f}'.
    '\x{e5a}\x{e5b}\x{f04}-\x{f12}\x{f3a}-\x{f3d}\x{f85}\x{104a}-\x{104f}\x{10fb}'.
    '\x{1361}-\x{1368}\x{166d}\x{166e}\x{169b}\x{169c}\x{16eb}-\x{16ed}\x{1735}'.
    '\x{1736}\x{17d4}-\x{17d6}\x{17d8}-\x{17da}\x{1800}-\x{180a}\x{1944}\x{1945}'.
    '\x{2010}-\x{2027}\x{2030}-\x{2043}\x{2045}-\x{2051}\x{2053}\x{2054}\x{2057}'.
    '\x{207d}\x{207e}\x{208d}\x{208e}\x{2329}\x{232a}\x{23b4}-\x{23b6}\x{2768}-'.
    '\x{2775}\x{27e6}-\x{27eb}\x{2983}-\x{2998}\x{29d8}-\x{29db}\x{29fc}\x{29fd}'.
    '\x{3001}-\x{3003}\x{3008}-\x{3011}\x{3014}-\x{301f}\x{3030}\x{303d}\x{30a0}'.
    '\x{30fb}\x{fd3e}\x{fd3f}\x{fe30}-\x{fe52}\x{fe54}-\x{fe61}\x{fe63}\x{fe68}'.
    '\x{fe6a}\x{fe6b}\x{ff01}-\x{ff03}\x{ff05}-\x{ff0a}\x{ff0c}-\x{ff0f}\x{ff1a}'.
    '\x{ff1b}\x{ff1f}\x{ff20}\x{ff3b}-\x{ff3d}\x{ff3f}\x{ff5b}\x{ff5d}\x{ff5f}-'.
    '\x{ff65}');

/**
 * Matches all CJK characters that are candidates for auto-splitting
 * (Chinese, Japanese, Korean).
 * Contains kana and BMP ideographs.
 */
define('PREG_CLASS_CJK', '\x{3041}-\x{30ff}\x{31f0}-\x{31ff}\x{3400}-\x{4db5}'.
    '\x{4e00}-\x{9fbb}\x{f900}-\x{fad9}');


$supersearch_consumed_time = 0;


/**
 * Implementation of hook_update_index().
 */
function supersearch_update_index() {
    global $supersearch_consumed_time;
    $time_limit = (int)variable_get('search_cron_time_limit_mins', 0)*60 + (int)variable_get('search_cron_time_limit_secs', 50) - $supersearch_consumed_time;
    if ($time_limit > 0) {
    // Store the maximum possible comments per thread (used for ranking by reply count)
        variable_set('node_cron_comments_scale', 1.0 / max(1, db_result(db_query('SELECT MAX(comment_count) FROM {node_comment_statistics}'))));
        variable_set('node_cron_views_scale', 1.0 / max(1, db_result(db_query('SELECT MAX(totalcount) FROM {node_counter}'))));

        $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {search_dataset} d ON d.type = 'node' AND d.sid = n.nid WHERE d.sid IS NULL OR d.reindex <> 0 ORDER BY d.reindex ASC, n.nid ASC");

        $start_time = time();
        while ($node = db_fetch_object($result)) {
            _node_index_node($node);
            if (time()-$start_time >= $time_limit)
                break;
        }
    }
}

/**
 * This function is called on shutdown to ensure that search_total is always
 * up to date (even if cron times out or otherwise fails).
 */
function supersearch_update_totals() {
// Update word IDF (Inverse Document Frequency) counts for new/changed words
    foreach (supersearch_dirty() as $word => $dummy) {
    // Get total count
        $total = db_result(db_query("SELECT SUM(score) FROM {search_index} WHERE word = '%s'", $word));
        // Apply Zipf's law to equalize the probability distribution
        $total = log10(1 + 1/(max(1, $total)));
        db_query("UPDATE {search_total} SET count = %f WHERE word = '%s'", $total, $word);
        if (!db_affected_rows()) {
            db_query("INSERT INTO {search_total} (word, count) VALUES ('%s', %f)", $word, $total);
        }
    }
    // Find words that were deleted from search_index, but are still in
    // search_total. We use a LEFT JOIN between the two tables and keep only the
    // rows which fail to join.
    $result = db_query("SELECT t.word AS realword, i.word FROM {search_total} t LEFT JOIN {search_index} i ON t.word = i.word WHERE i.word IS NULL");
    while ($word = db_fetch_object($result)) {
        db_query("DELETE FROM {search_total} WHERE word = '%s'", $word->realword);
    }
}

/**
 * Simplifies a string according to indexing rules.
 */
function supersearch_simplify($text) {
// Decode entities to UTF-8
    $text = decode_entities($text);

    // Lowercase
    $text = drupal_strtolower($text);

    // Call an external processor for word handling.
    supersearch_invoke_preprocess($text);

    // Simple CJK handling
    if (variable_get('overlap_cjk', TRUE)) {
        $text = preg_replace_callback('/['. PREG_CLASS_CJK .']+/u', 'supersearch_expand_cjk', $text);
    }

    // To improve searching for numerical data such as dates, IP addresses
    // or version numbers, we consider a group of numerical characters
    // separated only by punctuation characters to be one piece.
    // This also means that searching for e.g. '20/03/1984' also returns
    // results with '20-03-1984' in them.
    // Readable regexp: ([number]+)[punctuation]+(?=[number])
    $text = preg_replace('/(['. PREG_CLASS_NUMBERS .']+)['. PREG_CLASS_PUNCTUATION .']+(?=['. PREG_CLASS_NUMBERS .'])/u', '\1', $text);

    // The dot, underscore and dash are simply removed. This allows meaningful
    // search behavior with acronyms and URLs.
    $text = preg_replace('/[._-]+/', '', $text);

    // With the exception of the rules above, we consider all punctuation,
    // marks, spacers, etc, to be a word boundary.
    $text = preg_replace('/['. PREG_CLASS_SEARCH_EXCLUDE .']+/u', ' ', $text);

    return $text;
}

/**
 * Basic CJK tokenizer. Simply splits a string into consecutive, overlapping
 * sequences of characters ('minimum_word_size' long).
 */
function supersearch_expand_cjk($matches) {
    $min = variable_get('minimum_word_size', 3);
    $str = $matches[0];
    $l = drupal_strlen($str);
    // Passthrough short words
    if ($l <= $min) {
        return ' '. $str .' ';
    }
    $tokens = ' ';
    // FIFO queue of characters
    $chars = array();
    // Begin loop
    for ($i = 0; $i < $l; ++$i) {
    // Grab next character
        $current = drupal_substr($str, 0, 1);
        $str = substr($str, strlen($current));
        $chars[] = $current;
        if ($i >= $min - 1) {
            $tokens .= implode('', $chars) .' ';
            array_shift($chars);
        }
    }
    return $tokens;
}

/**
 * Splits a string into tokens for indexing.
 */
function supersearch_index_split($text) {
    static $last = NULL;
    static $lastsplit = NULL;

    if ($last == $text) {
        return $lastsplit;
    }
    // Process words
    $text = supersearch_simplify($text);
    $words = explode(' ', $text);
    array_walk($words, '_supersearch_index_truncate');

    // Save last keyword result
    $last = $text;
    $lastsplit = $words;

    return $words;
}

/**
 * Helper function for array_walk in supersearch_index_split.
 */
function _supersearch_index_truncate(&$text) {
    $text = truncate_utf8($text, 50);
}


class PDOInsertCache
{
    
  private $query = '';
  private $query_size = 0;
  private $query_prefix = 'INSERT INTO {$db_prefix}search_index (word, sid, type, score) VALUES';
  private $query_suffix = ' ON DUPLICATE KEY UPDATE score = score + VALUES(score)';
  private $max_query_size = 0;
  private $pdoresource;
  function __construct($pdo_string, $db_user, $db_pass)
  {
    $this->pdoresource = new PDO($pdo_string, $db_user, $db_pass);
    global $db_prefix;
    $this->query_prefix = str_replace('{$db_prefix}', $db_prefix, $this->query_prefix);
    $this->max_query_size = 1024*1024 - (strlen($query_prefix) + strlen($query_suffix));
  }
  function __destruct()
  {
      $this->flush();
      $this->pdoresource = NULL;
  }
  function flush()
  {
      if ($this->query_size > 0)
      {
        $this->pdoresource->exec($this->query_prefix . rtrim($this->query, ",") . $this->query_suffix);
        $this->query = '';
        $this->query_size = 0;
      }
  }
  function add_insert($word, $sid, $type, $score)
  {
      $querypart = "('{$word}', {$sid}, '{$type}', {$score}),";
      $querypart_length = strlen($querypart);
      if (($querypart_length + $this->query_size) > $this->max_query_size) $this->flush();
      $this->query .= $querypart;
      $this->query_size += $querypart_length;
      supersearch_dirty($word);
  }

}

/**
 * Update the full-text search index for a particular item.
 *
 * @param $sid
 *   A number identifying this particular item (e.g. node id).
 *
 * @param $type
 *   A string defining this type of item (e.g. 'node')
 *
 * @param $text
 *   The content of this item. Must be a piece of HTML text.
 *
 * @ingroup search
 */
function supersearch_index($sid, $type, $text) {
    $minimum_word_size = variable_get('minimum_word_size', 3);

    // Link matching
    global $base_url;
    $node_regexp = '@href=[\'"]?(?:'. preg_quote($base_url, '@') .'/|'. preg_quote(base_path(), '@') .')(?:\?q=)?/?((?![a-z]+:)[^\'">]+)[\'">]@i';

    // Multipliers for scores of words inside certain HTML tags.
    // Note: 'a' must be included for link ranking to work.
    $tags = array('h1' => 25,
        'h2' => 18,
        'h3' => 15,
        'h4' => 12,
        'h5' => 9,
        'h6' => 6,
        'u' => 3,
        'b' => 3,
        'i' => 3,
        'strong' => 3,
        'em' => 3,
        'a' => 10);

    // Strip off all ignored tags to speed up processing, but insert space before/after
    // them to keep word boundaries.
    $text = str_replace(array('<', '>'), array(' <', '> '), $text);
    $text = strip_tags($text, '<'. implode('><', array_keys($tags)) .'>');

    // Split HTML tags from plain text.
    $split = preg_split('/\s*<([^>]+?)>\s*/', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
    // Note: PHP ensures the array consists of alternating delimiters and literals
    // and begins and ends with a literal (inserting $null as required).

    $tag = FALSE; // Odd/even counter. Tag or no tag.
    $link = FALSE; // State variable for link analyser
    $score = 1; // Starting score per word
    $accum = ' '; // Accumulator for cleaned up data
    $tagstack = array(); // Stack with open tags
    $tagwords = 0; // Counter for consecutive words
    $focus = 1; // Focus state

    $results = array(0 => array()); // Accumulator for words for index

    foreach ($split as $value) {
        if ($tag) {
        // Increase or decrease score per word based on tag
            list($tagname) = explode(' ', $value, 2);
            $tagname = drupal_strtolower($tagname);
            // Closing or opening tag?
            if ($tagname[0] == '/') {
                $tagname = substr($tagname, 1);
                // If we encounter unexpected tags, reset score to avoid incorrect boosting.
                if (!count($tagstack) || $tagstack[0] != $tagname) {
                    $tagstack = array();
                    $score = 1;
                }
                else {
                // Remove from tag stack and decrement score
                    $score = max(1, $score - $tags[array_shift($tagstack)]);
                }
                if ($tagname == 'a') {
                    $link = FALSE;
                }
            }
            else {
                if (isset($tagstack[0]) && $tagstack[0] == $tagname) {
                // None of the tags we look for make sense when nested identically.
                // If they are, it's probably broken HTML.
                    $tagstack = array();
                    $score = 1;
                }
                else {
                // Add to open tag stack and increment score
                    array_unshift($tagstack, $tagname);
                    $score += $tags[$tagname];
                }
                if ($tagname == 'a') {
                // Check if link points to a node on this site
                    if (preg_match($node_regexp, $value, $match)) {
                        $path = drupal_get_normal_path($match[1]);
                        if (preg_match('!(?:node|book)/(?:view/)?([0-9]+)!i', $path, $match)) {
                            $linknid = $match[1];
                            if ($linknid > 0) {
                            // Note: ignore links to uncachable nodes to avoid redirect bugs.
                                $node = db_fetch_object(db_query('SELECT n.title, n.nid, n.vid, r.format FROM {node} n INNER JOIN {node_revisions} r ON n.vid = r.vid WHERE n.nid = %d', $linknid));
                                if (filter_format_allowcache($node->format)) {
                                    $link = TRUE;
                                    $linktitle = $node->title;
                                }
                            }
                        }
                    }
                }
            }
            // A tag change occurred, reset counter.
            $tagwords = 0;
        }
        else {
        // Note: use of PREG_SPLIT_DELIM_CAPTURE above will introduce empty values
            if ($value != '') {
                if ($link) {
                // Check to see if the node link text is its URL. If so, we use the target node title instead.
                    if (preg_match('!^https?://!i', $value)) {
                        $value = $linktitle;
                    }
                }
                $words = supersearch_index_split($value);
                foreach ($words as $word) {
                // Add word to accumulator
                    $accum .= $word .' ';
                    $num = is_numeric($word);
                    // Check wordlength
                    if ($num || drupal_strlen($word) >= $minimum_word_size) {
                    // Normalize numbers
                        if ($num) {
                            $word = (int)ltrim($word, '-0');
                        }

                        // Links score mainly for the target.
                        if ($link) {
                            if (!isset($results[$linknid])) {
                                $results[$linknid] = array();
                            }
                            $results[$linknid][] = $word;
                            // Reduce score of the link caption in the source.
                            $focus *= 0.2;
                        }
                        // Fall-through
                        if (!isset($results[0][$word])) {
                            $results[0][$word] = 0;
                        }
                        $results[0][$word] += $score * $focus;

                        // Focus is a decaying value in terms of the amount of unique words up to this point.
                        // From 100 words and more, it decays, to e.g. 0.5 at 500 words and 0.3 at 1000 words.
                        $focus = min(1, .01 + 3.5 / (2 + count($results[0]) * .015));
                    }
                    $tagwords++;
                    // Too many words inside a single tag probably mean a tag was accidentally left open.
                    if (count($tagstack) && $tagwords >= 15) {
                        $tagstack = array();
                        $score = 1;
                    }
                }
            }
        }
        $tag = !$tag;
    }

    supersearch_wipe($sid, $type, TRUE);

    // Insert cleaned up data into dataset
    db_query("INSERT INTO {search_dataset} (sid, type, data, reindex) VALUES (%d, '%s', '%s', %d)", $sid, $type, $accum, 0);
    $exts = get_loaded_extensions();
    $standart_way = TRUE;
    global $db_type;
    if (($db_type == "mysqli" || $db_type == "mysql") && in_array('PDO', $exts)) {
        global $db_url, $db_prefix;
        $url = parse_url(is_array($db_url) ? $db_url['default'] : $db_url);
        $db_user = urldecode($url['user']);
        $db_pass = isset($url['pass']) ? urldecode($url['pass']) : NULL;
        $db_host = urldecode($url['host']);
        $db_port = isset($url['port']) ? urldecode($url['port']) : '';
        $db_path = ltrim(urldecode($url['path']), '/');
        $pdo_string = "mysql:host=" . $db_host . (!empty($db_port) ? ";port=" . db_port : "") . ";dbname=" . $db_path;

        try {
            $standart_way = FALSE;
            $dbh = new PDOInsertCache($pdo_string, $db_user, $db_pass);
            foreach ($results[0] as $word => $score)
            $dbh->add_insert($word, $sid, $type, $score);
            $dbh = NULL;
        } catch (PDOException $e) {
        //if not set PDO::ATTR_ERRMODE -> PDO::ERRMODE_EXCEPTION, it'll catch only
        //PDO constructor. If it catched - this code'll go to standart_way
        }
    }

    if ($standart_way)
    // Insert results into search index
        foreach ($results[0] as $word => $score) {
        // Try inserting first because this will succeed most times, but because
        // the database collates similar words (accented and non-accented), the
        // insert can fail, in which case we need to add the word scores together.
            @db_query("INSERT INTO {search_index} (word, sid, type, score) VALUES ('%s', %d, '%s', %f)", $word, $sid, $type, $score);
            if (!db_affected_rows()) {
                db_query("UPDATE {search_index} SET score = score + %f WHERE word = '%s' AND sid = %d AND type = '%s'", $score, $word, $sid, $type);
            }
            supersearch_dirty($word);
        }

    unset($results[0]);

    // Get all previous links from this item.
    $result = db_query("SELECT nid, caption FROM {search_node_links} WHERE sid = %d AND type = '%s'", $sid, $type);
    $links = array();
    while ($link = db_fetch_object($result)) {
        $links[$link->nid] = $link->caption;
    }

    // Now store links to nodes.
    foreach ($results as $nid => $words) {
        $caption = implode(' ', $words);
        if (isset($links[$nid])) {
            if ($links[$nid] != $caption) {
            // Update the existing link and mark the node for reindexing.
                db_query("UPDATE {search_node_links} SET caption = '%s' WHERE sid = %d AND type = '%s' AND nid = %d", $caption, $sid, $type, $nid);
                supersearch_touch_node($nid);
            }
            // Unset the link to mark it as processed.
            unset($links[$nid]);
        }
        else {
        // Insert the existing link and mark the node for reindexing.
            db_query("INSERT INTO {search_node_links} (caption, sid, type, nid) VALUES ('%s', %d, '%s', %d)", $caption, $sid, $type, $nid);
            supersearch_touch_node($nid);
        }
    }
    // Any left-over links in $links no longer exist. Delete them and mark the nodes for reindexing.
    foreach ($links as $nid => $caption) {
        db_query("DELETE FROM {search_node_links} WHERE sid = %d AND type = '%s' AND nid = %d", $sid, $type, $nid);
        supersearch_touch_node($nid);
    }
supersearch_results_cache_clear();

}



/**
 * Marks a word as dirty (or retrieves the list of dirty words). This is used
 * during indexing (cron). Words which are dirty have outdated total counts in
 * the search_total table, and need to be recounted.
 */
function supersearch_dirty($word = NULL) {
    static $dirty = array();
    if ($word !== NULL) {
        $dirty[$word] = TRUE;
    }
    else {
        return $dirty;
    }
}



/**
 * Invokes hook_search_preprocess() in modules.
 */
function supersearch_invoke_preprocess(&$text) {
    require_once('supersearch.basic-admin.inc');
    supersearch_indexer_search_preprocess_run(&$text);
}





function supersearch_indexer_exception($id) {
    $module = db_result(db_query_range("SELECT module_name FROM {search_indexer_enabled_mods_hooks} WHERE id=%d", $id, 0, 1));
    drupal_set_title(drupal_get_title() . $module);
    $result = db_query("SELECT * FROM {watchdog} WHERE type='searchpreprocess' OR type='updateindex' ORDER BY wid DESC");
    $exceptions = array();
    while ($row = db_fetch_array($result)) {
        $variables = unserialize($row["variables"]);
        if ($variables["@module"]==$module)
            $exceptions[$row["wid"]]= array("type" => $row["type"], "message" => $variables["@message"], "location" => $row["location"], "timestamp" => $row["timestamp"]);
    }

    $header = array(t('Log ID'), t('Type'), t('Message'), t('Location link'), t('Created'));
    $rows = array();

    if (empty($exceptions))
        $rows[] = array(array('data' => t("No catched exceptions in $module module"), 'colspan' => 5));
    else
        foreach ($exceptions as $wid => $exception) {
            $rows[] = array(
                check_plain($wid),
                check_plain($exception["type"] == "searchpreprocess" ? "search_preprocess" : "update_index"),
                check_plain($exception["message"]),
                check_plain($exception["location"]),
                check_plain(strftime("%Y-%m-%d %H:%M:%S", $exception["timestamp"])),
            );
        }

    $output = theme('table', $header, $rows);
    return $output;
}


function supersearch_get_run_indexer() {
    header('Content-Type: text/plain');
    header('Connection: close');
    echo 'Running Indexer';
    register_shutdown_function('supersearch_update_totals');
    supersearch_update_index();
    //module_invoke('supersearch', 'update_index');
    //supersearch_indexer_update_index_run();
    echo 'Done';
    exit();
}

function supersearch_indexer_modules_list_from_db($sort = 0) {
    return db_query("SELECT * FROM {search_indexer_enabled_mods_hooks} ORDER BY weight ". ($sort==0 ? "ASC" : "DESC"));
}

function supersearch_indexer_modules_list() {
  $modules1 = module_implements('search_preprocess', TRUE);
  $modules2 = module_implements('update_index', TRUE);
  $mods = array_merge($modules1, $modules2);
  sort($mods);
  $modules = array();
  foreach ($mods as $module) {
    $modules[$module] = array("search_preprocess" => in_array($module, $modules1),
                              "update_index" => in_array($module, $modules2));
  }
  unset($modules1);
  unset($modules2);
  unset($mods);
  return $modules;
}

function supersearch_indexer_add_new_modules($modules) {
  $max_weight = db_result(db_query("SELECT MAX(weight) FROM {search_indexer_enabled_mods_hooks}"));
  if ($max_weight == NULL)
    $max_weight = 0;
  else
    $max_weight++;

  foreach ($modules as $module => $hooks) {
    $is = db_result(db_query_range("SELECT COUNT(*) FROM {search_indexer_enabled_mods_hooks} WHERE module_name='%s'", $module, 0, 1));
    if (!$is) {
      db_query("INSERT INTO {search_indexer_enabled_mods_hooks} (module_name, weight, enabled_search_preprocess, enabled_update_index) VALUES ('%s', %d, %d, %d)", $module, $max_weight, $hooks["search_preprocess"], $hooks["update_index"]);
      $max_weight++;
    }
  }
}

function supersearch_indexer_module_set_statistics($module, $start_time, $end_time, $hook_name) {
    $interval = $end_time - $start_time;
    $max_exec_interval = db_result(db_query_range("SELECT max_{$hook_name}_exec_interval FROM {search_indexer_enabled_mods_hooks} WHERE module_name='%s'", $module, 0, 1));
    if ($max_exec_interval < $interval)
        $max_exec_interval = ", max_{$hook_name}_exec_interval=" . $interval;
    else
        $max_exec_interval = "";
    db_query("UPDATE {search_indexer_enabled_mods_hooks}
            SET last_{$hook_name}_exec_interval=%d,
            total_{$hook_name}_exec_interval=total_{$hook_name}_exec_interval+%d,
            times_{$hook_name}_exec_count=times_{$hook_name}_exec_count+1{$max_exec_interval} WHERE module_name='%s'", $interval, $interval, $module);
}



function supersearch_indexer_update_index_run() {
    set_time_limit(3600);
    // We register a shutdown function to ensure that search_total is always up
    // to date.
    register_shutdown_function('supersearch_update_totals');

    // Update word index
    $modules = supersearch_indexer_modules_list();
    supersearch_indexer_add_new_modules($modules);
    $result = supersearch_indexer_modules_list_from_db();
    //cleanDump($result);

    global $supersearch_consumed_time;
    while ($module = db_fetch_object($result)) {
        if ($module->enabled_update_index != 1)
            continue;
        try {
            $start_time = time();
            module_invoke($module->module_name, 'update_index');
            $supersearch_consumed_time =+ time() - $start_time;
            supersearch_indexer_module_set_statistics($module->module_name, $start_time, time(), 'update_index');
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $is_search_preprocess = FALSE;
            $file = basename($e->getFile());
            $module_name = substr($file, 0, strpos($file, '.'));
            foreach ($e->getTrace() as $trace) {
                if (strpos($trace['function'], "_search_preprocess") !== FALSE) {
                    $is_search_preprocess = TRUE;
                    break;
                }
            }

            $type = $is_search_preprocess ? "searchpreprocess" : "updateindex";
            watchdog(t($type), t("Module name: @module;\nException message: @message;"), array('@module' => $module_name, '@message' =>  !empty($message) ? $message : "no message"), WATCHDOG_ERROR);
            throw new Exception("superindexer exception");
        }
    }
}

function supersearch_indexer_search_preprocess_run(&$text) {
    global $supersearch_consumed_time;
    $result = supersearch_indexer_modules_list_from_db();

    while ($module=db_fetch_object($result)) {
        if ($module->enabled_search_preprocess != 1)
            continue;
        $start_time = time();
        $text = module_invoke($module->module_name, 'search_preprocess', $text);
        $supersearch_consumed_time =+ time() - $start_time;
        supersearch_indexer_module_set_statistics($module->module_name, $start_time, time(), 'search_preprocess');
    }
}


